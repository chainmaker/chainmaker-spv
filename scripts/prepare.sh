#!/usr/bin/env bash

#
# Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

# 部署spv
deploy_spv(){
  # 1.创建build/release目录结构
  rm -rf ${DUMP_PATH}
  mkdir -p ${DUMP_PATH}/bin
  mkdir -p ${DUMP_PATH}/config

  # 2.编译生成spv二进制文件
  cd ../main && go build -o spv
  mv spv ${DUMP_PATH}/bin && cd - > /dev/null
  cp -rf local/* ${DUMP_PATH}/bin/

  # 3.复制config文件
  cp -rf ../config/* ${DUMP_PATH}/config/

  echo "Prepare Finished!"
}

# output root dir
DUMP_PATH="../build/release"

deploy_spv
