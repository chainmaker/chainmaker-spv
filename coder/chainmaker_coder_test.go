/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package coder

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/pb/protogo"
	"github.com/stretchr/testify/require"
)

func TestChainMakerCoder_Func(t *testing.T) {
	conf.BlockChainConfigs["chain1"] = &protogo.ChainConfig{
		ChainId:  "chain1",
		HashType: "SHA256",
	}
	cr := NewChainMakerCoder()

	headerBytes, err := cr.SerializeBlockHeader(&common.CMBlockHeader{
		BlockHeader: &cmCommonPb.BlockHeader{},
	})
	require.NotNil(t, headerBytes)
	require.Nil(t, err)

	header, err := cr.DeserializeBlockHeader(headerBytes)
	require.NotNil(t, header)
	require.Nil(t, err)

	transaction := &cmCommonPb.Transaction{
		Header: &cmCommonPb.TxHeader{
			ChainId: "chain1",
		},
		Result: &cmCommonPb.Result{
			ContractResult: &cmCommonPb.ContractResult{},
		},
	}

	txHashMap, err := cr.GenerateTransactionHashes(&common.CMBlock{
		Block: &cmCommonPb.Block{
			Txs: []*cmCommonPb.Transaction{
				transaction,
			},
		},
	})
	require.NotNil(t, txHashMap)
	require.Nil(t, err)
}
