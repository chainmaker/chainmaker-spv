/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package coder

import (
	"errors"
	"fmt"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/utils"
	"github.com/gogo/protobuf/proto"
)

// ChainMakerCoder is the implementation of Coder interface for ChainMaker
type ChainMakerCoder struct{}

// NewChainMakerCoder creates ChainMakerCoder
func NewChainMakerCoder() *ChainMakerCoder {
	coder := &ChainMakerCoder{}
	return coder
}

// SerializeBlockHeader serializes ChainMaker's block header to binary bytes
func (cmc *ChainMakerCoder) SerializeBlockHeader(header common.Header) ([]byte, error) {
	if header == nil {
		return nil, errors.New("serialize header is nil")
	}

	cmBlockHeader, ok := header.(*common.CMBlockHeader)
	if !ok {
		return nil, fmt.Errorf("type conversion failed when serialize header, from Header to CMBlockHeader")
	}

	blockHeader := cmBlockHeader.BlockHeader
	blockHeaderBytes, err := proto.Marshal(blockHeader)
	if err != nil {
		return nil, err
	}

	return blockHeaderBytes, nil
}

// DeserializeBlockHeader deserializes binary bytes to ChainMaker's block header
func (cmc *ChainMakerCoder) DeserializeBlockHeader(serializeBlockHeader []byte) (common.Header, error) {
	if serializeBlockHeader == nil {
		return nil, errors.New("deserialize header is nil")
	}

	blockHeader := &cmCommonPb.BlockHeader{}
	err := proto.Unmarshal(serializeBlockHeader, blockHeader)
	if err != nil {
		return nil, err
	}

	cmBlockHeader := &common.CMBlockHeader{
		BlockHeader: blockHeader,
	}
	return cmBlockHeader, nil
}

// GenerateTransactionHashes generates transaction hash map with the transactions in block
func (cmc *ChainMakerCoder) GenerateTransactionHashes(blocker common.Blocker) (map[string][]byte, error) {
	if blocker == nil {
		return nil, errors.New("blocker is nil when generate transaction hash")
	}

	cmBlock, ok := blocker.(*common.CMBlock)
	if !ok {
		return nil, fmt.Errorf("type conversion failed when generate transaction hash, from Blocker to CMBlock")
	}

	txs := cmBlock.Block.Txs

	txHashMap := make(map[string][]byte, len(txs))

	for _, tx := range txs {
		txKey := tx.GetHeader().GetTxId()
		txHash, err := utils.CalcTxHash(tx)
		if err != nil {
			return nil, err
		}
		txHashMap[txKey] = txHash
	}
	return txHashMap, nil
}
