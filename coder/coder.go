/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package coder defines coding and decoding interface and contains the implementations of each chain
package coder

import "chainmaker.org/chainmaker-spv/common"

// Coder defines coding and decoding interface
type Coder interface {
	// SerializeBlockHeader serializes block header to binary bytes
	SerializeBlockHeader(header common.Header) ([]byte, error)

	// DeserializeBlockHeader deserializes binary bytes to block header
	DeserializeBlockHeader(serializeBlockHeader []byte) (common.Header, error)

	// GenerateTransactionHashes generates transaction hash map with the transactions in block, map[txKey]=txHash
	GenerateTransactionHashes(blocker common.Blocker) (map[string][]byte, error)
}
