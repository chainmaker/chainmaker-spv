/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package mock implements the interface in sdk.go and sdk_interface.go
package mock

import (
	"reflect"

	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/pb/protogo"
	"github.com/golang/mock/gomock"
)

// MockSDKAdapter is a mock of SDKAdapter interface.
type MockSDKAdapter struct {
	ctrl     *gomock.Controller
	recorder *MockSDKAdapterMockRecorder
}

// MockSDKAdapterMockRecorder is the mock recorder for MockSDKAdapter.
type MockSDKAdapterMockRecorder struct {
	mock *MockSDKAdapter
}

// NewMockSDKAdapter creates a new mock instance.
func NewMockSDKAdapter(ctrl *gomock.Controller) *MockSDKAdapter {
	mock := &MockSDKAdapter{ctrl: ctrl}
	mock.recorder = &MockSDKAdapterMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockSDKAdapter) EXPECT() *MockSDKAdapterMockRecorder {
	return m.recorder
}

// GetBlockByHeight mocks base method.
func (m *MockSDKAdapter) GetBlockByHeight(blockHeight int64) (common.Blocker, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetBlockByHeight", blockHeight)
	ret0, _ := ret[0].(common.Blocker)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetBlockByHeight indicates an expected call of GetBlockByHeight.
func (mr *MockSDKAdapterMockRecorder) GetBlockByHeight(blockHeight interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetBlockByHeight", reflect.TypeOf((*MockSDKAdapter)(nil).GetBlockByHeight), blockHeight)
}

// GetChainConfig mocks base method.
func (m *MockSDKAdapter) GetChainConfig() (*protogo.ChainConfig, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetChainConfig")
	ret0, _ := ret[0].(*protogo.ChainConfig)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetChainConfig indicates an expected call of GetChainConfig.
func (mr *MockSDKAdapterMockRecorder) GetChainConfig() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetChainConfig", reflect.TypeOf((*MockSDKAdapter)(nil).GetChainConfig))
}

// GetChainHeight mocks base method.
func (m *MockSDKAdapter) GetChainHeight() (int64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetChainHeight")
	ret0, _ := ret[0].(int64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetChainHeight indicates an expected call of GetChainHeight.
func (mr *MockSDKAdapterMockRecorder) GetChainHeight() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetChainHeight", reflect.TypeOf((*MockSDKAdapter)(nil).GetChainHeight))
}

// GetTransactionByTxKey mocks base method.
func (m *MockSDKAdapter) GetTransactionByTxKey(txKey string) (common.Transactioner, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetTransactionByTxKey", txKey)
	ret0, _ := ret[0].(common.Transactioner)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetTransactionByTxKey indicates an expected call of GetTransactionByTxKey.
func (mr *MockSDKAdapterMockRecorder) GetTransactionByTxKey(txKey interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetTransactionByTxKey", reflect.TypeOf((*MockSDKAdapter)(nil).GetTransactionByTxKey), txKey)
}

// Stop mocks base method.
func (m *MockSDKAdapter) Stop() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Stop")
	ret0, _ := ret[0].(error)
	return ret0
}

// Stop indicates an expected call of Stop.
func (mr *MockSDKAdapterMockRecorder) Stop() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Stop", reflect.TypeOf((*MockSDKAdapter)(nil).Stop))
}

// SubscribeBlock mocks base method.
func (m *MockSDKAdapter) SubscribeBlock() (chan common.Blocker, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SubscribeBlock")
	ret0, _ := ret[0].(chan common.Blocker)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// SubscribeBlock indicates an expected call of SubscribeBlock.
func (mr *MockSDKAdapterMockRecorder) SubscribeBlock() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SubscribeBlock", reflect.TypeOf((*MockSDKAdapter)(nil).SubscribeBlock))
}
