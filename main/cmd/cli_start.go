/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package cmd

import (
	"os"
	"os/signal"
	"syscall"

	"chainmaker.org/chainmaker-spv/logger"
	"chainmaker.org/chainmaker-spv/rpcserver"
	"chainmaker.org/chainmaker-spv/server"
	"github.com/spf13/cobra"
)

var log = logger.GetLogger(logger.ModuleCli)

func StartCMD() *cobra.Command {
	startCmd := &cobra.Command{
		Use:   "start",
		Short: "Startup SPV Server",
		Long:  "Startup ChainMaker SPV Server",
		RunE: func(cmd *cobra.Command, _ []string) error {
			initSpvConfig(cmd)
			start()
			return nil
		},
	}
	attachFlags(startCmd, []string{flagNameOfConfigFilepath})
	return startCmd
}

func start() {
	// 1. init spv server
	spvServer, err := server.IntSPVServer()
	if err != nil {
		log.Errorf("Init SPV Servers Failed! err msg:%s", err.Error())
		return
	}

	if err = spvServer.Start(); err != nil {
		log.Errorf("Start SPV Server Failed! err msg:%s", err.Error())
		return
	}

	// 2. init grpc server
	rpcServer := rpcserver.NewRPCServer(spvServer)

	if err = rpcServer.Start(); err != nil {
		log.Errorf("Startup GRPC Server Failed! err msg:%s", err.Error())
	}

	// 3. process exit signal
	errorC := make(chan error, 1)
	go handleExitSignal(errorC)
	// nolint
	select {
	case <-errorC:
		rpcServer.Stop()
		if err := spvServer.Stop(); err != nil {
			log.Errorf("Stop SPV Server Failed! err msg:%s", err.Error())
		}
	}
}

func handleExitSignal(exitC chan<- error) {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM, os.Interrupt, syscall.SIGINT)
	defer signal.Stop(signalChan)

	for sig := range signalChan {
		log.Infof("received signal:%d (%s)", sig, sig)
		exitC <- nil
	}
}
