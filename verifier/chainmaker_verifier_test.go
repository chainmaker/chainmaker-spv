/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package verifier

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/pb/protogo"
	"github.com/stretchr/testify/require"
)

func TestChainMakerVerifier_ValidBlock(t *testing.T) {
	conf.BlockChainConfigs["chain1"] = &protogo.ChainConfig{
		HashType: "SHA256",
	}
	tx := &cmCommonPb.Transaction{
		Header: &cmCommonPb.TxHeader{
			ChainId: "chain1",
		},
		Result: &cmCommonPb.Result{},
	}
	vfr := NewChainMakerVerifier()
	err := vfr.ValidBlock(&common.CMBlock{
		Block: &cmCommonPb.Block{
			Header: &cmCommonPb.BlockHeader{
				ChainId: "chain1",
				TxCount: 1,
				TxRoot:  []byte{50, 66, 13, 18, 146, 68, 60, 116, 203, 212, 101, 213, 115, 215, 171, 218, 120, 118, 151, 129, 80, 3, 48, 184, 98, 106, 131, 109, 103, 147, 134, 56},
				DagHash: []byte{227, 176, 196, 66, 152, 252, 28, 20, 154, 251, 244, 200, 153, 111, 185, 36, 39, 174, 65, 228, 100, 155, 147, 76, 164, 149, 153, 27, 120, 82, 184, 85},
			},
			Dag: &cmCommonPb.DAG{},
			Txs: []*cmCommonPb.Transaction{
				tx,
			},
		},
	}, []byte{})
	require.Nil(t, err)
}
