/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package verifier

import (
	"bytes"
	"errors"
	"fmt"

	"chainmaker.org/chainmaker-go/common/crypto/hash"
	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/utils"
)

// ChainMakerVerifier implements block verifier interface for ChainMaker
type ChainMakerVerifier struct{}

// NewChainMakerVerifier creates ChainMakerVerifier
func NewChainMakerVerifier() *ChainMakerVerifier {
	verifier := &ChainMakerVerifier{}
	return verifier
}

// ValidBlock verifies the validity of the block for ChainMaker
func (cmv *ChainMakerVerifier) ValidBlock(blocker common.Blocker, preBlockHash []byte) error {
	if blocker == nil || preBlockHash == nil {
		return errors.New("blocker or pre block hash is nil")
	}
	cmBlock, ok := blocker.(*common.CMBlock)
	if !ok {
		return fmt.Errorf(" Type conversion failed, from Blocker to CMBlock")
	}

	block := cmBlock.Block
	return cmv.validBlock(block, preBlockHash)
}

func (cmv *ChainMakerVerifier) validBlock(block *cmCommonPb.Block, preBlockHash []byte) error {
	// 1.verify pre hash
	if !bytes.Equal(preBlockHash, block.GetHeader().GetPreBlockHash()) {
		return errors.New("block pre hash unequal")
	}

	// 2.If the block is empty, there is no need to validate other fields in the block header
	if block.Header.TxCount == 0 {
		return nil
	}

	// 3.If the block is not empty, three trees will be validated
	if block.Header.TxCount != int64(len(block.Txs)) {
		return fmt.Errorf("block height [%d], block hash [%x] header txcount [%d], txs [%d]",
			block.Header.BlockHeight, block.Header.BlockHash, block.Header.TxCount, len(block.Txs))
	}

	hashType := conf.BlockChainConfigs[block.Header.ChainId].HashType

	txHashes := make([][]byte, 0)
	// verify TxRoot
	for _, tx := range block.Txs {
		txHash, err := utils.CalcTxHash(tx)
		if err != nil {
			return errors.New("calc hash tx failed")
		}
		txHashes = append(txHashes, txHash)
	}
	txRoot, err := hash.GetMerkleRoot(hashType, txHashes)
	if err != nil || !bytes.Equal(txRoot, block.GetHeader().GetTxRoot()) {
		return fmt.Errorf("calc merkleroot failed, block height [%d], header hash [%x], txroot [%x], header txroot [%x]",
			block.Header.BlockHeight, block.Header.BlockHash, txRoot, block.Header.TxRoot)
	}

	// verify DAG hash
	dagHash, err := utils.CalcDagHash(hashType, block.GetDag())
	if err != nil || !bytes.Equal(dagHash, block.Header.DagHash) {
		return fmt.Errorf("calc daghash failed, block height [%d], block hash [%x], daghash [%x], header dag hash [%x]",
			block.Header.BlockHeight, block.Header.BlockHash, dagHash, block.Header.DagHash)

	}

	// verify read write set
	rwSetRoot, err := utils.CalcRWSetRoot(hashType, block.GetTxs())
	if err != nil {
		return fmt.Errorf("calc rwsetroot failed, block height [%d], block hash [%x], err: %s",
			block.Header.BlockHeight, block.Header.BlockHash, err)
	}
	if !bytes.Equal(rwSetRoot, block.Header.RwSetRoot) {
		return fmt.Errorf("rwsetroot unequal, block height [%d], block hash[%x], header rwsetroot [%x], rwsetroot [%x]",
			block.Header.BlockHeight, block.Header.BlockHash, block.Header.RwSetRoot, rwSetRoot)
	}

	return nil
}
