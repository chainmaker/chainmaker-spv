/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package rpcserver

import (
	"context"
	"time"

	"chainmaker.org/chainmaker-spv/pb/api"
	"chainmaker.org/chainmaker-spv/server"
)

// ApiService contains SPVServer and implements RpcProverServer interface
type ApiService struct {
	spvServer *server.SPVServer
	ctx       context.Context
}

// NewApiService creates ApiService
func NewApiService(ctx context.Context, spvServer *server.SPVServer) *ApiService {
	apiService := &ApiService{
		spvServer: spvServer,
		ctx:       ctx,
	}

	return apiService
}

// ValidTransaction is the implementation of RpcProverServer interface
func (s *ApiService) ValidTransaction(ctx context.Context, info *api.TxValidationInfo) (
	*api.TxValidationResponse, error) {
	var (
		timeout  = 5000 * time.Millisecond
		response *api.TxValidationResponse
		err      error
	)

	if info == nil {
		response = &api.TxValidationResponse{
			ChainId: "",
			TxKey:   "",
			Code:    api.StatusCode_INVALID,
			Message: "TxValidationInfo is nil",
		}
		return response, nil
	}

	err = s.spvServer.ValidTransaction(info, timeout)
	if err != nil {
		response = &api.TxValidationResponse{
			ChainId: info.ChainId,
			TxKey:   info.TxKey,
			Code:    api.StatusCode_INVALID,
			Message: err.Error(),
		}
		return response, nil
	}

	response = &api.TxValidationResponse{
		ChainId: info.ChainId,
		TxKey:   info.TxKey,
		Code:    api.StatusCode_VALID,
		Message: "valid transaction",
	}
	return response, nil
}
