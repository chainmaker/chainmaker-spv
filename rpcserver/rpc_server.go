/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package rpcserver is gRPC server module
package rpcserver

import (
	"context"
	"net"
	"strconv"

	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/logger"
	"chainmaker.org/chainmaker-spv/pb/api"
	"chainmaker.org/chainmaker-spv/server"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

// RPCServer is gRPC server module
type RPCServer struct {
	grpcServer *grpc.Server
	spvServer  *server.SPVServer
	ctx        context.Context
	cancel     context.CancelFunc
	log        *zap.SugaredLogger
}

// NewRPCServer creates RPCServer
func NewRPCServer(spvServer *server.SPVServer) *RPCServer {
	rpcServer := &RPCServer{
		grpcServer: grpc.NewServer(),
		spvServer:  spvServer,
		log:        logger.GetLogger(logger.ModuleRpc),
	}
	return rpcServer
}

// Start startups gRPC Server
func (rs *RPCServer) Start() error {
	var (
		err error
	)

	rs.ctx, rs.cancel = context.WithCancel(context.Background())

	rs.registerHandler()

	endPort := conf.SPVConfig.GRPCConfig.Address + ":" + strconv.Itoa(conf.SPVConfig.GRPCConfig.Port)
	conn, err := net.Listen("tcp", endPort)
	if err != nil {
		rs.log.Errorf("GRPC Connection Failed! err msg: %rs", err.Error())
		return err
	}

	go func() {
		err = rs.grpcServer.Serve(conn)
		if err != nil {
			rs.log.Errorf("Startup GRPC Server Failed! err msg: %rs", err.Error())
		}
	}()
	rs.log.Infof("GRPC Server Listen on %s", endPort)

	return nil
}

// Stop stops gRPC Server
func (rs *RPCServer) Stop() {
	rs.cancel()
	rs.grpcServer.Stop()
	rs.log.Infof("GRPC Server is Stopped!")
}

// registerHandler registers gRPC Server
func (rs *RPCServer) registerHandler() {
	apiService := NewApiService(rs.ctx, rs.spvServer)
	api.RegisterRpcProverServer(rs.grpcServer, apiService)
}
