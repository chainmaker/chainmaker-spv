/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package rpcserver

import (
	"testing"

	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/server"
	"github.com/stretchr/testify/require"
)

func TestRpcServer_Func(t *testing.T) {
	conf.SPVConfig.GRPCConfig = &conf.GRPCConfig{
		Address: "localhost",
		Port:    0,
	}
	rpcSrv := NewRPCServer(&server.SPVServer{})
	err := rpcSrv.Start()
	require.Nil(t, err)
	rpcSrv.Stop()
}
