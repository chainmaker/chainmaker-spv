/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package rpcserver

import (
	"context"
	"testing"

	"chainmaker.org/chainmaker-spv/pb/api"
	"chainmaker.org/chainmaker-spv/server"
	"github.com/stretchr/testify/require"
)

func TestApiServer_Func(t *testing.T) {
	apiSrv := NewApiService(context.Background(), &server.SPVServer{})
	resp, err := apiSrv.ValidTransaction(context.Background(), &api.TxValidationInfo{})
	require.NotNil(t, resp)
	require.Nil(t, err)
}
