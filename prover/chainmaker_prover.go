/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package prover

import (
	"bytes"
	"fmt"
	"time"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/adapter"
	"chainmaker.org/chainmaker-spv/coder"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/logger"
	"chainmaker.org/chainmaker-spv/manager"
	"chainmaker.org/chainmaker-spv/pb/api"
	"chainmaker.org/chainmaker-spv/storage"
	"github.com/Rican7/retry"
	"github.com/Rican7/retry/strategy"
	"go.uber.org/zap"
)

const (
	retryCnt       = 10                      // the maximum number of polling transaction
	retryInterval  = 5000                    // the time interval of polling transaction. unit: ms
	defaultTimeout = 5000 * time.Millisecond // the timeout of synchronizing expected block. unit: ms
)

// ChainMakerProver is the implementation of Prover interface for ChainMaker
type ChainMakerProver struct {
	chainId      string
	sdkAdapter   adapter.SDKAdapter
	coder        coder.Coder
	store        storage.StateDB
	blockManager *manager.BlockManager
	log          *zap.SugaredLogger
}

// NewChainMakerProver creates ChainMakerProver
func NewChainMakerProver(chainId string, sdkAdapter adapter.SDKAdapter, coder coder.Coder, store storage.StateDB,
	blockManager *manager.BlockManager, log *zap.SugaredLogger) *ChainMakerProver {
	if log == nil {
		log = logger.GetLogger(logger.ModuleProver)
	}

	prover := &ChainMakerProver{
		chainId:      chainId,
		sdkAdapter:   sdkAdapter,
		coder:        coder,
		store:        store,
		blockManager: blockManager,
		log:          log,
	}
	return prover
}

// ValidTransaction verifies the existence and validity of the transaction
func (p *ChainMakerProver) ValidTransaction(txInfo *api.TxValidationInfo, timeout time.Duration) error {
	var (
		blockHeight  int64
		txKey        string
		contractData = &api.ContractData{}
		err          error
	)

	// 0. check param
	if err = p.verifyInfo(txInfo); err != nil {
		p.log.Errorf("[ChainId:%s] param is invalid! err msg:%s", p.chainId, err.Error())
		return fmt.Errorf("[ChainId:%s] param is invalid! err msg:%s", p.chainId, err.Error())
	}

	blockHeight = txInfo.BlockHeight
	txKey = txInfo.TxKey
	contractData = txInfo.ContractData

	// 1. If the transaction in a higher block than spv local current height,
	// prover will register a listener to listen the missing block.
	_, lastCommittedBlockHeight, ok := p.store.GetLastCommittedBlockHeaderAndHeight(p.chainId)
	if !ok {
		return fmt.Errorf("[ChainId:%s] prover get last committed block header and height failed", p.chainId)
	}

	if lastCommittedBlockHeight < blockHeight {
		if timeout < defaultTimeout {
			timeout = defaultTimeout
		}

		listener := make(chan int64, 1)
		bHeight := p.blockManager.RegisterListener(blockHeight, listener)
		if bHeight == -1 {
			select {
			case height := <-listener:
				p.log.Debugf("[ChainId:%s] spv has synced to height:%d", p.chainId, height)
			case <-time.After(timeout):
				p.log.Debugf("[ChainId:%s] spv cannot sync to height:%d before timeout", p.chainId, blockHeight)
			}
			p.blockManager.RemoveListener(blockHeight)
		}
	}

	// 2. According to TxId obtain the transaction hash and the transaction at the height of the block
	localTxHash, localHeight, ok := p.store.GetTransactionInfoByTxKey(p.chainId, txKey)
	if !ok {
		return fmt.Errorf("[ChainId:%s]transaction no exist, transaction key:%s, current local block height:%d",
			p.chainId, txKey, lastCommittedBlockHeight)
	}

	if blockHeight != localHeight {
		p.log.Errorf("[ChainId:%s] invalid transaction! actual height:%d, expected block height:%d",
			p.chainId, localHeight, blockHeight)
		return fmt.Errorf("[ChainId:%s] invalid transaction! actual height:%d, expected height:%d",
			p.chainId, localHeight, blockHeight)
	}

	// 3. get transaction by sdk from remote chain
	var (
		txer       common.Transactioner
		remoteHash []byte
	)
	err = retry.Retry(func(uint) error {
		txer, err = p.sdkAdapter.GetTransactionByTxKey(txKey)
		if err != nil {
			p.log.Warnf("[ChainId:%s] prover request transaction failed! transaction key:%s", p.chainId, txKey)
			return err
		}
		// 4. transaction existence validation：verify whether the transaction hash is consistent
		remoteHash, err = txer.GetTransactionHash()
		if err != nil {
			return err
		}

		if !bytes.Equal(localTxHash, remoteHash) {
			p.log.Warnf("[ChainId:%s] prover gets invalid transaction by SDK! remote hash:%s, local hash:%s",
				p.chainId, remoteHash, localTxHash)
			return fmt.Errorf("[ChainId:%s] prover gets invalid transaction by SDK! remote hash:%s, local hash:%s",
				p.chainId, remoteHash, localTxHash)
		}

		return nil
	},
		strategy.Limit(retryCnt),
		strategy.Wait(retryInterval*time.Millisecond),
	)

	if err != nil {
		p.log.Errorf("[ChainId:%s] prover can't get valid transaction by SDK! transaction key:%s",
			p.chainId, txKey)
		return fmt.Errorf(fmt.Sprintf("[ChainId:%s] prover can't get transaction by SDK! transaction key:%s",
			p.chainId, txKey))
	}

	// 5. transaction validity validation：judge whether transaction status code is represented as a valid transaction
	if status := txer.GetStatusCode(); status != 0 {
		p.log.Errorf("[ChainId:%s] invalid transaction! transaction status code:%d", p.chainId, status)
		return fmt.Errorf("[ChainId:%s] invalid transaction! transaction status code:%d", p.chainId, status)
	}

	// 6. contract data validation：judge whether contract data is consistent, including contract name, method, params
	var (
		expectedContractName string
		expectedMethod       string
		expectedParams       []*api.KVPair
	)
	expectedContractName = contractData.ContractName
	expectedMethod = contractData.Method
	expectedParams = contractData.Params

	actualContractName, err := txer.GetContractName()
	if err != nil {
		return fmt.Errorf("[ChainId:%s] prover gets actual transaction contract name failed, err:%s", p.chainId, err.Error())
	}

	actualMethod, err := txer.GetMethod()
	if err != nil {
		return fmt.Errorf("[ChainId:%s] prover gets actual transaction contract method failed, err:%s",
			p.chainId, err.Error())
	}

	actualParameters, err := txer.GetParams()
	if err != nil {
		return fmt.Errorf("[ChainId:%s] prover gets actual transaction contract params failed, err:%s",
			p.chainId, err.Error())
	}

	if actualContractName != expectedContractName {
		p.log.Errorf("[ChainId:%s] invalid transaction! different contract name, actual name:%s, expected name:%s",
			p.chainId, actualContractName, expectedContractName)
		return fmt.Errorf("[ChainId:%s] invalid transaction! different contract name, actual name:%s, expected name:%s",
			p.chainId, actualContractName, expectedContractName)
	}

	if actualMethod != expectedMethod {
		p.log.Errorf("[ChainId:%s] invalid transaction! different contract method, actual method:%s, expected method:%s",
			p.chainId, actualMethod, expectedMethod)
		return fmt.Errorf("[ChainId:%s] invalid transaction! different contract method, actual method:%s, expected method:%s",
			p.chainId, actualMethod, expectedMethod)
	}

	if len(actualParameters) == 0 && isEmptyParams(expectedParams) {
		p.log.Infof("[ChainId:%s] prover verifies a valid transaction, txKey:%s", p.chainId, txKey)
		return nil
	}

	if len(actualParameters) != len(expectedParams) {
		p.log.Errorf("[ChainId:%s] invalid transaction! different param len, actual params:%v, "+
			"expected params:%v", p.chainId, actualParameters, expectedParams)
		return fmt.Errorf("[ChainId:%s] invalid transaction! different param len, actual params:%v, "+
			"expected params:%v", p.chainId, actualParameters, expectedParams)
	}

	var actualParams []*cmCommonPb.KeyValuePair
	for _, pair := range actualParameters {
		param, ok := pair.(*cmCommonPb.KeyValuePair)
		if !ok {
			return fmt.Errorf("[ChainId:%s] prover type conversion failed! from interface to KeyValuePair", p.chainId)
		}
		actualParams = append(actualParams, param)
	}

	for i := 0; i < len(expectedParams) && i < len(actualParams); i++ {
		if expectedParams[i].Key != actualParams[i].Key || expectedParams[i].Value != actualParams[i].Value {
			p.log.Errorf("[ChainId:%s] invalid transaction! actual param: [%s, %s], expected param: [%s, %s]",
				p.chainId, actualParams[i].Key, actualParams[i].Value, expectedParams[i].Key, expectedParams[i].Value)
			return fmt.Errorf("[ChainId:%s] invalid transaction! actual param: [%s, %s], expected param: [%s, %s]",
				p.chainId, actualParams[i].Key, actualParams[i].Value, expectedParams[i].Key, expectedParams[i].Value)
		}
	}

	p.log.Infof("[ChainId:%s] prover verifies a valid transaction, txKey:%s", p.chainId, txKey)
	return nil
}

// verifyInfo judges TxValidationInfo
func (p *ChainMakerProver) verifyInfo(info *api.TxValidationInfo) error {
	if info == nil {
		return fmt.Errorf("TxValidationInfo is nil")
	}

	if info.ChainId != p.chainId {
		return fmt.Errorf("ChainId should be %s", p.chainId)
	}

	if info.BlockHeight < 0 {
		return fmt.Errorf("BlockHeight should be greater than 0")
	}

	if info.TxKey == "" {
		return fmt.Errorf("TxKey is \"\" ")
	}

	if info.ContractData == nil {
		return fmt.Errorf("ContractData is nil")
	}

	if info.ContractData.ContractName == "" {
		return fmt.Errorf("ContractName is \"\" ")
	}

	if info.ContractData.Method == "" {
		return fmt.Errorf("Method is \"\" ")
	}

	return nil
}

// isEmptyParams judges whether the param is empty
func isEmptyParams(params []*api.KVPair) bool {
	if len(params) == 0 {
		return true
	}

	for _, param := range params {
		if param.Key != "" || param.Value != "" {
			return false
		}
	}
	return true
}
