/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package prover

import (
	"testing"
	"time"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/coder"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/manager"
	"chainmaker.org/chainmaker-spv/mock"
	"chainmaker.org/chainmaker-spv/pb/api"
	"chainmaker.org/chainmaker-spv/pb/protogo"
	"chainmaker.org/chainmaker-spv/storage/kvdb/leveldb"
	"chainmaker.org/chainmaker-spv/verifier"
	"github.com/gogo/protobuf/proto"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestChainMakerProver_Func(t *testing.T) {
	cc := &conf.ChainConfig{
		ChainId:                    "chain1",
		SyncChainInfoIntervalMills: 1000,
		SDKConfigPath:              "../config/chainmaker_sdk_config_chain1.yml",
	}
	conf.SPVConfig = &conf.Config{
		Chains: []*conf.ChainConfig{
			cc,
		},
		StorageConfig: &conf.StoreConfig{
			Provider: "leveldb",
			LevelDB: &conf.LevelDBConfig{
				StorePath:       "../data/spv_db",
				WriteBufferSize: 4,
				BloomFilterBits: 10,
			},
		},
	}
	conf.BlockChainConfigs["chain1"] = &protogo.ChainConfig{
		HashType: "SHA256",
	}

	chainID := "chain1"
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	sdkAdapter := mock.NewMockSDKAdapter(ctrl)
	param := &cmCommonPb.KeyValuePair{
		Key:   "key1",
		Value: "value1",
	}
	queryOrTransactPayload := cmCommonPb.QueryPayload{
		ContractName: "contract1",
		Method:       "unknown",
		Parameters: []*cmCommonPb.KeyValuePair{
			param,
		},
	}
	pl, _ := proto.Marshal(&queryOrTransactPayload)
	sdkAdapter.EXPECT().GetTransactionByTxKey(gomock.Eq("tx1")).Return(&common.CMTransaction{
		Transaction: &cmCommonPb.Transaction{
			Header: &cmCommonPb.TxHeader{
				ChainId: "chain1",
			},
			RequestPayload: pl,
		},
	}, nil).AnyTimes()
	cdr := coder.NewChainMakerCoder()
	store := leveldb.NewKvStateDB(conf.SPVConfig.StorageConfig, nil)
	cmStateManager := manager.NewStateManager(conf.SPVConfig.Chains[0].ChainId, sdkAdapter, verifier.NewChainMakerVerifier(), cdr, store, nil)

	chainProver := NewChainMakerProver(chainID, sdkAdapter, cdr, store, cmStateManager.GetBlockManager(), nil)

	store.CommitBlockHeaderAndTxHashes(chainID, 2, []byte("chain1 Header"), map[string][]byte{
		"tx1": {109, 36, 223, 137, 153, 170, 131, 73, 193, 83, 30, 218, 1, 113, 187, 78, 97, 154, 219, 45, 89, 196, 197, 157, 20, 243, 67, 80, 103, 152, 42, 170},
		"tx2": []byte("tx2's hash"),
		"tx3": []byte("tx3's hash"),
	})

	param1 := &api.KVPair{
		Key:   "key1",
		Value: "value1",
	}
	err := chainProver.ValidTransaction(&api.TxValidationInfo{
		ChainId:     chainID,
		TxKey:       "tx1",
		BlockHeight: 2,
		ContractData: &api.ContractData{
			ContractName: "contract1",
			Method:       "unknown",
			Params: []*api.KVPair{
				param1,
			},
		},
	}, time.Duration(5)*time.Second)
	require.Nil(t, err)
}
