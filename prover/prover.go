/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package prover is transaction validation module
package prover

import (
	"time"

	"chainmaker.org/chainmaker-spv/pb/api"
)

// Prover defines transaction validation interface
type Prover interface {
	// ValidTransaction verifies the existence and validity of the transaction
	ValidTransaction(txInfo *api.TxValidationInfo, timeout time.Duration) error
}
