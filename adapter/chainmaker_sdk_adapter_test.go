/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package adapter

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-sdk-go/pb/protogo/config"
	"chainmaker.org/chainmaker-sdk-go/pb/protogo/discovery"
	"chainmaker.org/chainmaker-spv/logger"
	"chainmaker.org/chainmaker-spv/mock"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func Test_NewChainMakerSDKAdapter(t *testing.T) {
	configFile := "../config/chainmaker_sdk_config_chain2.yml"
	NewChainMakerSDKAdapter("chain2", configFile, logger.GetDefaultLogger())
}

func TestChainMakerSDKAdapter_Func(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock.NewMockSDKInterface(ctrl)
	apt := &ChainMakerSDKAdapter{
		chainId:       "chain1",
		CMChainClient: m,
	}
	defer apt.Stop()
	m.EXPECT().Stop().Return(nil)
	m.EXPECT().GetChainInfo().AnyTimes().Return(&discovery.ChainInfo{BlockHeight: 1}, nil)
	m.EXPECT().GetChainConfig().AnyTimes().Return(&config.ChainConfig{
		Crypto: &config.CryptoConfig{
			Hash: "sha256",
		},
	}, nil)
	m.EXPECT().GetBlockByHeight(gomock.Eq(int64(1)), gomock.Any()).AnyTimes().Return(&cmCommonPb.BlockInfo{
		Block: &cmCommonPb.Block{
			Header: &cmCommonPb.BlockHeader{
				BlockHeight: 1,
			},
		},
	}, nil)
	m.EXPECT().GetTxByTxId(gomock.Eq("000")).AnyTimes().Return(&cmCommonPb.TransactionInfo{
		Transaction: &cmCommonPb.Transaction{
			Result: &cmCommonPb.Result{
				Code: cmCommonPb.TxStatusCode_SUCCESS,
			},
		},
	}, nil)
	m.EXPECT().SubscribeBlock(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().Return(make(chan interface{}, 1), nil)

	height, err := apt.GetChainHeight()
	require.Equal(t, height, int64(1))
	require.Nil(t, err)

	config, err := apt.GetChainConfig()
	require.Equal(t, config.HashType, "sha256")
	require.Nil(t, err)

	blocker, err := apt.GetBlockByHeight(int64(1))
	require.Equal(t, blocker.GetHeight(), int64(1))
	require.Nil(t, err)

	txer, err := apt.GetTransactionByTxKey("000")
	require.Equal(t, txer.GetStatusCode(), int32(cmCommonPb.TxStatusCode_SUCCESS))
	require.Nil(t, err)

	bCh, err := apt.SubscribeBlock()
	require.Equal(t, len(bCh), 0)
	require.Nil(t, err)
}
