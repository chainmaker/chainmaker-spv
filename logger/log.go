/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package logger

import (
	"sync"

	"go.uber.org/zap"
)

// module log name
const (
	ModuleCli          = "[Cli]"
	ModuleSpv          = "[SPV]"
	ModuleStateManager = "[StateManager]"
	ModuleReqManager   = "[ReqManager]"
	ModuleBlockManager = "[BlockManager]"
	ModuleStateDB      = "[StateDB]"
	ModuleProver       = "[Prover]"
	ModuleRpc          = "[Rpc]"
	ModuleSdk          = "[Sdk]"
)

var (
	loggers     = make(map[string]*zap.SugaredLogger)
	logConfig   *LogConfig
	loggerMutex sync.Mutex
)

// SetLogConfig sets the config of logger module, called in initialization of config module
func SetLogConfig(config *LogConfig) {
	logConfig = config
}

// GetLogger finds or creates a logger with module name, usually called in initialization of all module.
func GetLogger(name string) *zap.SugaredLogger {
	loggerMutex.Lock()
	defer loggerMutex.Unlock()
	var config Config
	logHeader := name
	logger, ok := loggers[logHeader]
	if !ok {
		if logConfig == nil {
			logConfig = defaultLogConfig()
		}
		config = Config{
			Module:       name,
			LogPath:      logConfig.SystemLog.FilePath,
			LogLevel:     GetLogLevel(logConfig.SystemLog.LogLevel),
			MaxAge:       logConfig.SystemLog.MaxAge,
			RotationTime: logConfig.SystemLog.RotationTime,
			JsonFormat:   false,
			ShowLine:     true,
			LogInConsole: logConfig.SystemLog.LogInConsole,
			ShowColor:    logConfig.SystemLog.ShowColor,
		}

		logger = initSugarLogger(&config)
		loggers[logHeader] = logger
	}
	return logger
}

// GetDefaultLogger creates default logger
func GetDefaultLogger() *zap.SugaredLogger {
	defaultConfig := defaultLogConfig()
	config := &Config{
		Module:       "[SPV]",
		LogPath:      defaultConfig.SystemLog.FilePath,
		LogLevel:     GetLogLevel(defaultConfig.SystemLog.LogLevel),
		MaxAge:       defaultConfig.SystemLog.MaxAge,
		RotationTime: defaultConfig.SystemLog.RotationTime,
		JsonFormat:   false,
		ShowLine:     true,
		LogInConsole: defaultConfig.SystemLog.LogInConsole,
		ShowColor:    defaultConfig.SystemLog.ShowColor,
	}

	return initSugarLogger(config)
}

// defaultLogConfig creates default config for logger module
func defaultLogConfig() *LogConfig {
	config := &LogConfig{
		SystemLog: LogNodeConfig{
			LogLevel:     INFO,
			FilePath:     "../log/default_spv.log",
			MaxAge:       DefaultMaxAge,
			RotationTime: DefaultRotationTime,
			LogInConsole: false,
			ShowColor:    true,
		},
	}
	return config
}
