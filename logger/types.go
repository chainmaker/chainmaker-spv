/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package logger

// LogConfig is the config of logger module
type LogConfig struct {
	ConfigFile string        `mapstructure:"config_file"`
	SystemLog  LogNodeConfig `mapstructure:"system"`
}

// LogNodeConfig is the logger config of node
type LogNodeConfig struct {
	LogLevel     string `mapstructure:"log_level"`
	FilePath     string `mapstructure:"file_path"`
	MaxAge       int    `mapstructure:"max_age"`
	RotationTime int    `mapstructure:"rotation_time"`
	LogInConsole bool   `mapstructure:"log_in_console"`
	ShowColor    bool   `mapstructure:"show_color"`
}
