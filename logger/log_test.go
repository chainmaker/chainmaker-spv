/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package logger

import (
	"testing"

	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

func Test_SetLogConfig(t *testing.T) {
	logConfig := &LogConfig{
		SystemLog: LogNodeConfig{
			LogLevel:     INFO,
			FilePath:     "../log/default_spv.log",
			MaxAge:       DefaultMaxAge,
			RotationTime: DefaultRotationTime,
			LogInConsole: false,
			ShowColor:    true,
		},
	}
	SetLogConfig(logConfig)
}

func Test_GetLogger(t *testing.T) {
	var log *zap.SugaredLogger
	log = GetLogger(ModuleCli)
	require.NotNil(t, log)

	log = GetLogger(ModuleSpv)
	require.NotNil(t, log)

	log = GetLogger(ModuleStateManager)
	require.NotNil(t, log)

	log = GetLogger(ModuleReqManager)
	require.NotNil(t, log)

	log = GetLogger(ModuleBlockManager)
	require.NotNil(t, log)

	log = GetLogger(ModuleStateDB)
	require.NotNil(t, log)

	log = GetLogger(ModuleProver)
	require.NotNil(t, log)

	log = GetLogger(ModuleRpc)
	require.NotNil(t, log)

	log = GetLogger(ModuleSdk)
	require.NotNil(t, log)
}

func Test_GetDefaultLogger(t *testing.T) {
	log := GetDefaultLogger()
	require.NotNil(t, log)
}
