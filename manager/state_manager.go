/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package manager contains block synchronization logic
package manager

import (
	"context"
	"fmt"
	"time"

	"chainmaker.org/chainmaker-spv/adapter"
	"chainmaker.org/chainmaker-spv/coder"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/logger"
	"chainmaker.org/chainmaker-spv/pb/protogo"
	"chainmaker.org/chainmaker-spv/storage"
	"chainmaker.org/chainmaker-spv/verifier"
	"github.com/Rican7/retry"
	"github.com/Rican7/retry/strategy"
	"github.com/gogo/protobuf/proto"
	"go.uber.org/zap"
)

const (
	defaultBlockerChanSize            = 2048  // default size of blockerCh
	defaultHeightMsgChanSize          = 1024  // default size of heightMsgCh
	defaultSyncChainInfoIntervalMills = 10000 // default interval for polling the height of remote chain，unit：ms
)

// StateManager is the block synchronization module
type StateManager struct {
	chainId     string
	reqManger   *ReqManager
	blockManger *BlockManager
	cache       *Cache
	sdkAdapter  adapter.SDKAdapter
	store       storage.StateDB

	blockerCh         chan common.Blocker
	heightMsgCh       chan *HeightMsg
	listenSubscribeCh chan bool
	ctx               context.Context
	cancel            context.CancelFunc

	log *zap.SugaredLogger
}

// HeightMsg is the block height information missing from spv
type HeightMsg struct {
	height       int64
	isFromRemote bool // indicating that the missing height information is coming from the remote chain or within the spv
}

// NewStateManager creates StateManger
func NewStateManager(chainId string, sdkAdapter adapter.SDKAdapter, verifier verifier.Verifier, coder coder.Coder,
	store storage.StateDB, log *zap.SugaredLogger) *StateManager {
	bCh := make(chan common.Blocker, defaultBlockerChanSize)
	hCh := make(chan *HeightMsg, defaultHeightMsgChanSize)
	ctx, cancel := context.WithCancel(context.Background())

	// synchronize blocks from genesis block when setting currentHeight=-1, requestedMaxHeight=-1, remoteMaxHeight=0
	cache := NewCache(-1, -1, 0)

	// new BlockManager
	bm := NewBlockManager(ctx, chainId, cache, verifier, coder, store, bCh, hCh, log)

	// new ReqManager
	rm := NewReqManager(ctx, chainId, cache, sdkAdapter, bCh, hCh, log)

	if log == nil {
		log = logger.GetLogger(logger.ModuleStateManager)
	}

	// new StateManager
	sm := &StateManager{
		chainId:           chainId,
		reqManger:         rm,
		blockManger:       bm,
		cache:             cache,
		sdkAdapter:        sdkAdapter,
		store:             store,
		blockerCh:         bCh,
		heightMsgCh:       hCh,
		listenSubscribeCh: make(chan bool),
		ctx:               ctx,
		cancel:            cancel,
		log:               log,
	}
	return sm
}

// initStateManger init StateManger including querying remote chain config
// and handling restart process
func (sm *StateManager) initStateManger() error {

	// get chain config from remote chain
	cc, err := sm.getChainConfig()
	if err != nil {
		return err
	}

	conf.BlockChainConfigs[cc.ChainId] = cc

	ccBytes, err := proto.Marshal(cc)
	if err != nil {
		return err
	}

	err = sm.store.WriteChainConfig(sm.chainId, ccBytes)
	if err != nil {
		return fmt.Errorf("[ChainId:%s] write chain config failed", sm.chainId)
	}

	// restart process
	if _, height, ok := sm.store.GetLastCommittedBlockHeaderAndHeight(sm.chainId); ok {
		if err = sm.cache.SetCurrentHeight(height); err != nil {
			return err
		}
		if err = sm.cache.SetRequestedMaxHeight(height); err != nil {
			return err
		}
		if err = sm.cache.SetRemoteMaxHeight(height); err != nil {
			return err
		}
		sm.log.Infof("[ChainId:%s] Restart spv from block height:%d", sm.chainId, height)
	}

	return nil
}

// getChainConfig gets chain config by SDK
func (sm *StateManager) getChainConfig() (*protogo.ChainConfig, error) {
	var (
		err error
		cc  *protogo.ChainConfig
	)
	err = retry.Retry(func(uint) error {
		cc, err = sm.sdkAdapter.GetChainConfig()
		if err != nil {
			sm.log.Warnf("[ChainId:%s] get chain config failed! err:%s", sm.chainId, err.Error())
			return err
		}

		if cc.ChainId != sm.chainId {
			sm.log.Errorf("[ChainId:%s] inconsistent chain id between chain config and state manager", sm.chainId)
			return err
		}

		return nil
	},
		strategy.Limit(retryCnt),
		strategy.Wait(retryInterval*time.Millisecond),
	)

	if err != nil {
		sm.log.Panicf("[ChainId:%s] get chain config failed! err: %s", sm.chainId, err.Error())
		panic(fmt.Sprintf("[ChainId:%s] get chain config failed! err: %s", sm.chainId, err.Error()))
	}
	return cc, nil
}

// Start startups StateManager
func (sm *StateManager) Start() error {
	var err error
	// init state manager
	err = sm.initStateManger()
	if err != nil {
		sm.log.Debugf("[ChainId:%s] Init state manager failed!", sm.chainId)
		return err
	}
	sm.log.Infof("[ChainId:%s] Start state manager!", sm.chainId)

	// subscribe the latest block
	err = sm.SubscribeBlock()
	if err != nil {
		return err
	}

	// monitor and resubscribe block when the connection is broken
	sm.MonitorSubscribeBlock()

	// poll the latest block height of the remote chain periodically
	sm.StartupChainInfoLoop()

	// startup request manager
	sm.reqManger.Start()

	// startup block manager
	sm.blockManger.Start()

	sm.log.Debugf("[ChainId:%s] Finish start state manager!", sm.chainId)
	return nil
}

// SubscribeBlock subscribes the latest block from remote chain
func (sm *StateManager) SubscribeBlock() error {
	ch, err := sm.sdkAdapter.SubscribeBlock()
	if err != nil {
		sm.log.Errorf("[ChainId:%s] subscribe block failed!", sm.chainId)
		return err
	}
	sm.log.Infof("[ChainId:%s] subscribe block successfully!", sm.chainId)

	go func() {
		for {
			select {
			case blocker, ok := <-ch:
				// connection is broken and the channel of subscribing block is closed
				if !ok {
					sm.log.Error(fmt.Sprintf("[ChainId:%s] SubscribeBlock channel has been closed", sm.chainId))
					sm.listenSubscribeCh <- true
					return
				}
				if blocker.GetHeight() > sm.cache.GetRequestedMaxHeight() {
					sm.log.Debugf("[ChainId:%s] receive a block by subscribe, block height:%d",
						sm.chainId, blocker.GetHeight())
					sm.blockerCh <- blocker
				}
				if blocker.GetHeight() > sm.cache.GetRemoteMaxHeight() {
					err := sm.cache.SetRemoteMaxHeight(blocker.GetHeight())
					if err != nil {
						sm.log.Warnf("[ChainId:%s] update remote max height failed! remote max height:%d, new height:%d",
							sm.chainId, sm.cache.GetRemoteMaxHeight(), blocker.GetHeight())
					}
					sm.log.Debugf("[ChainId:%s] receive a higher block by subscribe, new remote max height:%d",
						sm.chainId, sm.cache.GetRemoteMaxHeight())
				}
			case <-sm.ctx.Done():
				return
			}
		}
	}()

	return nil
}

// MonitorSubscribeBlock monitors and resubscribes block when the connection is broken
func (sm *StateManager) MonitorSubscribeBlock() {
	go func() {
		for {
			select {
			case <-sm.listenSubscribeCh:
				err := sm.SubscribeBlock()
				if err != nil {
					// resubscribe after 10000ms
					t := time.NewTicker(10000 * time.Millisecond)
					// nolint
					select {
					case <-t.C:
						go func() {
							sm.listenSubscribeCh <- true
						}()
					}
				}
			case <-sm.ctx.Done():
				return
			}

		}
	}()
}

// StartupChainInfoLoop polls the latest block height of the remote chain periodically
func (sm *StateManager) StartupChainInfoLoop() {
	var syncInterval int32
	for _, chain := range conf.SPVConfig.Chains {
		if chain.ChainId == sm.chainId {
			syncInterval = chain.SyncChainInfoIntervalMills
		}
	}

	if syncInterval < defaultSyncChainInfoIntervalMills {
		syncInterval = defaultSyncChainInfoIntervalMills
	}

	interval := time.Duration(syncInterval) * time.Millisecond
	t := time.NewTicker(interval)
	go func() {
		for {
			select {
			case <-t.C:
				if remoteHeight, err := sm.sdkAdapter.GetChainHeight(); err == nil {
					if remoteHeight == sm.cache.GetCurrentHeight() && remoteHeight == sm.cache.GetRemoteMaxHeight() {
						sm.log.Infof("[ChainId:%s] spv has synced to the highest block! current local height:%d, remote max height:%d",
							sm.chainId, sm.cache.GetCurrentHeight(), sm.cache.GetRemoteMaxHeight())
						break
					}

					if remoteHeight > sm.cache.GetRequestedMaxHeight() {
						hMsg := &HeightMsg{
							height:       remoteHeight,
							isFromRemote: true,
						}
						sm.log.Debugf("[ChainId:%s] receive a chainInfo by loop, height:%d", sm.chainId, remoteHeight)
						sm.heightMsgCh <- hMsg
					}

					if remoteHeight > sm.cache.GetRemoteMaxHeight() {
						err = sm.cache.SetRemoteMaxHeight(remoteHeight)
						if err != nil {
							sm.log.Warnf("[ChainId:%s] update remote max height failed! remote max height:%d, new height:%d",
								sm.chainId, sm.cache.GetRemoteMaxHeight(), remoteHeight)
						}
						sm.log.Debugf("[ChainId:%s] receive a higher chainInfo by loop, new remote max height:%d",
							sm.chainId, sm.cache.GetRemoteMaxHeight())
					}
				}
			case <-sm.ctx.Done():
				return
			}
		}
	}()
}

// GetBlockManager returns block manger
func (sm *StateManager) GetBlockManager() *BlockManager {
	return sm.blockManger
}

// Stop stops StateManager
func (sm *StateManager) Stop() error {
	defer sm.log.Infof("[ChainId:%s] Stop state manager!", sm.chainId)
	err := sm.sdkAdapter.Stop()
	if err != nil {
		sm.log.Error("[ChainId:%s] Stop sdk adapter failed!", sm.chainId)
	}

	sm.cancel()

	return nil
}
