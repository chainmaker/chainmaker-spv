/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package manager

import (
	"fmt"
	"sync"
)

// Cache caches the important variables during block synchronization
type Cache struct {
	currentHeight      int64 // the block height has synchronized and committed by spv
	requestedMaxHeight int64 // the block height has requested by spv
	remoteMaxHeight    int64 // the maximum block height of remote chain known to spv
	sync.RWMutex
}

// NewCache creates Cache
func NewCache(current int64, requestedHeight int64, remoteMaxHeight int64) *Cache {
	if current < 0 {
		current = -1
	}
	if requestedHeight < 0 {
		requestedHeight = -1
	}
	if remoteMaxHeight <= 0 {
		remoteMaxHeight = 0
	}
	cache := &Cache{
		currentHeight:      current,
		requestedMaxHeight: requestedHeight,
		remoteMaxHeight:    remoteMaxHeight,
	}
	return cache
}

// GetCurrentHeight returns currentHeight
func (c *Cache) GetCurrentHeight() int64 {
	c.RLock()
	defer c.RUnlock()
	return c.currentHeight
}

// GetRequestedMaxHeight returns requestedMaxHeight
func (c *Cache) GetRequestedMaxHeight() int64 {
	c.RLock()
	defer c.RUnlock()
	return c.requestedMaxHeight
}

// GetRemoteMaxHeight returns remoteMaxHeight
func (c *Cache) GetRemoteMaxHeight() int64 {
	c.RLock()
	defer c.RUnlock()
	return c.remoteMaxHeight
}

// SetCurrentHeight sets currentHeight
func (c *Cache) SetCurrentHeight(newCurrent int64) error {
	c.Lock()
	defer c.Unlock()
	if newCurrent < c.currentHeight {
		return fmt.Errorf("set current block height failed!, current height:%d, new height:%d", c.currentHeight, newCurrent)
	}
	c.currentHeight = newCurrent
	return nil
}

// SetRequestedMaxHeight sets requestedMaxHeight
func (c *Cache) SetRequestedMaxHeight(newRequestHeight int64) error {
	c.Lock()
	defer c.Unlock()
	if newRequestHeight < c.requestedMaxHeight {
		return fmt.Errorf("set has requested max height failed!, has requested max height:%d, new height:%d",
			c.requestedMaxHeight, newRequestHeight)
	}
	c.requestedMaxHeight = newRequestHeight
	return nil
}

// SetRemoteMaxHeight sets remoteMaxHeight
func (c *Cache) SetRemoteMaxHeight(newRemote int64) error {
	c.Lock()
	defer c.Unlock()
	if newRemote < c.remoteMaxHeight {
		return fmt.Errorf("set remote block height failed!, remote max height:%d, new height:%d",
			c.remoteMaxHeight, newRemote)
	}
	c.remoteMaxHeight = newRemote
	return nil
}
