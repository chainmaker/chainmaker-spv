/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package manager

import (
	"sync"
	"testing"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-sdk-go/pb/protogo/config"
	"chainmaker.org/chainmaker-sdk-go/pb/protogo/discovery"
	"chainmaker.org/chainmaker-spv/adapter"
	"chainmaker.org/chainmaker-spv/coder"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/logger"
	"chainmaker.org/chainmaker-spv/mock"
	"chainmaker.org/chainmaker-spv/storage/kvdb/leveldb"
	"chainmaker.org/chainmaker-spv/verifier"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestStateManager_Start(t *testing.T) {
	// 1. init SPV config
	err := conf.InitSPVConfigWithYmlFile("")
	require.Nil(t, err)

	// 2. new verifier, coder and store
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock.NewMockSDKInterface(ctrl)
	apt := &adapter.ChainMakerSDKAdapter{
		CMChainClient: m,
	}
	defer apt.Stop()
	m.EXPECT().Stop().AnyTimes().Return(nil)
	m.EXPECT().GetChainInfo().AnyTimes().Return(&discovery.ChainInfo{BlockHeight: 1}, nil)
	m.EXPECT().GetChainConfig().AnyTimes().Return(&config.ChainConfig{
		ChainId: "chain1",
		Crypto: &config.CryptoConfig{
			Hash: "SHA256"},
	}, nil)
	m.EXPECT().GetBlockByHeight(gomock.Any(), gomock.Any()).AnyTimes().Return(&cmCommonPb.BlockInfo{
		Block: newBlock(1),
	}, nil)
	m.EXPECT().GetTxByTxId(gomock.Any()).AnyTimes().Return(&cmCommonPb.TransactionInfo{}, nil)
	m.EXPECT().SubscribeBlock(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(
		newBlockCh(0), nil)

	// 3. new state manager, containing new block manager and new req manager
	cmVerifier := verifier.NewChainMakerVerifier()
	cmCoder := coder.NewChainMakerCoder()
	cmStore := leveldb.NewKvStateDB(conf.SPVConfig.StorageConfig, logger.GetLogger(logger.ModuleStateDB))
	require.NotNil(t, cmStore)

	sm := NewStateManager("chain1", apt, cmVerifier, cmCoder, cmStore, nil)
	require.NotNil(t, sm)

	// 4. start state manager, containing start block manager and start req manager
	err = sm.Start()

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			if sm.GetBlockManager().cache.GetCurrentHeight() >= 0 {
				break
			}
		}
	}()
	wg.Wait()
	require.Nil(t, err)

	// 5. register and remove listener
	h1 := sm.blockManger.RegisterListener(0, make(chan int64))
	require.Equal(t, h1, int64(0))

	sm.blockManger.RemoveListener(0)

	// 6. stop state manager
	err = sm.Stop()
	require.Nil(t, err)
}

func newBlock(height int64) *cmCommonPb.Block {
	block := &cmCommonPb.Block{
		Header: &cmCommonPb.BlockHeader{
			ChainId:      "chain1",
			BlockHeight:  height,
			PreBlockHash: []byte("pre_hash"),
		},
		Dag:            nil,
		Txs:            make([]*cmCommonPb.Transaction, 0),
		AdditionalData: nil,
	}

	return block
}

func newBlockCh(height int64) chan interface{} {
	ch := make(chan interface{}, 1)
	block0 := &cmCommonPb.BlockInfo{
		Block: newBlock(height),
	}
	ch <- block0

	return ch
}
