/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package manager

import (
	"context"
	"time"

	"chainmaker.org/chainmaker-spv/adapter"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/logger"
	"github.com/Rican7/retry"
	"github.com/Rican7/retry/strategy"
	"go.uber.org/zap"
	"golang.org/x/sync/semaphore"
)

const (
	retryCnt       = 10   // the number of repeated requests for chain config
	retryInterval  = 5000 // the time interval of repeated requests for a block or chain config，unit：ms
	defaultWorkers = 1024 // the maximum number of goroutines for concurrent request blocks
)

// ReqManager is the block request module
type ReqManager struct {
	chainId    string
	cache      *Cache
	sdkAdapter adapter.SDKAdapter

	blockerCh        chan common.Blocker
	heightMsgCh      chan *HeightMsg
	workersSemaphore *semaphore.Weighted
	ctx              context.Context

	log *zap.SugaredLogger
}

// NewReqManager creates ReqManager
func NewReqManager(ctx context.Context, chainId string, cache *Cache, sdkAdapter adapter.SDKAdapter,
	bCh chan common.Blocker, hCh chan *HeightMsg, log *zap.SugaredLogger) *ReqManager {
	if log == nil {
		log = logger.GetLogger(logger.ModuleReqManager)
	}
	rm := &ReqManager{
		chainId:          chainId,
		cache:            cache,
		sdkAdapter:       sdkAdapter,
		blockerCh:        bCh,
		heightMsgCh:      hCh,
		workersSemaphore: semaphore.NewWeighted(int64(defaultWorkers)),
		ctx:              ctx,
		log:              log,
	}
	return rm
}

// Start startups ReqManager module
func (rm *ReqManager) Start() {
	rm.log.Debugf("[ChainId:%s] start request manager!", rm.chainId)
	rm.receiveHeightMsg()
}

// receiveHeightMsg receives heightMsg from heightMsgCh
func (rm *ReqManager) receiveHeightMsg() {
	go func() {
		for {
			select {
			case hMsf, ok := <-rm.heightMsgCh:
				if !ok {
					rm.log.Warnf("[ChainId:%s] heightMsgCh has been closed!", rm.chainId)
					return
				}

				if err := rm.processHeightMsg(hMsf); err != nil {
					rm.log.Errorf("[ChainId:%s] process height msg failed!", rm.chainId)
					return
				}
			case <-rm.ctx.Done():
				return
			}
		}
	}()
}

// processHeightMsg process heightMsg received from heightMsgCh
func (rm *ReqManager) processHeightMsg(heightMsg *HeightMsg) error {
	var err error
	height := heightMsg.height
	isFromRemote := heightMsg.isFromRemote
	requestedMaxHeight := rm.cache.GetRequestedMaxHeight()

	// process the HeightMsg from remote chain, which needs request block from requestedMaxHeight to remote height
	if isFromRemote {
		if height <= requestedMaxHeight {
			rm.log.Debugf("[ChainId:%s] expired HeightMsg! has requested block height:%d, block height:%d",
				rm.chainId, requestedMaxHeight, height)
			return nil
		}

		for h := requestedMaxHeight + 1; h <= height; h++ {
			rh := h
			rm.requestBlockByHeight(rh)
		}

		err = rm.cache.SetRequestedMaxHeight(height)
		if err != nil {
			rm.log.Errorf("[ChainId:%s] set has requested max height failed! has requested block height:%d, new height:%d",
				rm.chainId, requestedMaxHeight, height)
			return err
		}
		rm.log.Debugf("[ChainId:%s] set has requested max height successfully! has requested block height:%d, new height:%d",
			rm.chainId, requestedMaxHeight, height)
		return nil
	}

	// process the HeightMsg from local by BlockManager, which only need request a block of this height
	rm.requestBlockByHeight(height)
	return nil
}

// requestBlockByHeight requests block by height until it arrives
func (rm *ReqManager) requestBlockByHeight(height int64) {
	// acquire semaphore
	err := rm.workersSemaphore.Acquire(context.Background(), 1)
	if err != nil {
		rm.log.Errorf("[ChainId:%s] get worker failed! block height:%d", rm.chainId, height)
	}

	go func() {
		var (
			block common.Blocker
			err   error
		)
		// always retry
		err = retry.Retry(func(uint) error {
			rm.log.Debugf("[ChainId:%s] request block! block height:%d", rm.chainId, height)
			block, err = rm.sdkAdapter.GetBlockByHeight(height)
			if err != nil {
				rm.log.Errorf("[ChainId:%s] request block failed! block height:%d", rm.chainId, height)
				return err
			}
			return nil
		},
			strategy.Wait(retryInterval*time.Millisecond),
		)

		if err != nil {
			rm.log.Errorf("[ChainId:%s] can't request and response block by SDK! block height:%d", rm.chainId, height)
		}

		rm.log.Debugf("[ChainId:%s] response block by request, request block height:%d, response block height:%d",
			rm.chainId, height, block.GetHeight())
		rm.blockerCh <- block

		// release semaphore
		rm.workersSemaphore.Release(1)
	}()
}
