/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package manager

import (
	"context"
	"fmt"
	"sync"

	"chainmaker.org/chainmaker-spv/coder"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/logger"
	"chainmaker.org/chainmaker-spv/storage"
	"chainmaker.org/chainmaker-spv/verifier"
	"go.uber.org/zap"
)

const (
	defaultBlockerMapSize = 2048 // default initial capacity of the map for caching synchronized blocks
)

// BlockManager is the block validation and submission module
type BlockManager struct {
	chainId  string
	cache    *Cache
	verifier verifier.Verifier
	coder    coder.Coder
	store    storage.StateDB

	blockerMap  map[int64]common.Blocker
	blockerCh   chan common.Blocker
	heightMsgCh chan *HeightMsg
	ctx         context.Context

	sync.Mutex
	lastCommittedBlockHeight int64
	listeners                map[int64]chan int64

	log *zap.SugaredLogger
}

// NewBlockManager creates BlockManger
func NewBlockManager(ctx context.Context, chainId string, cache *Cache, verifier verifier.Verifier, coder coder.Coder,
	store storage.StateDB, bCh chan common.Blocker, hCh chan *HeightMsg, log *zap.SugaredLogger) *BlockManager {
	if log == nil {
		log = logger.GetLogger(logger.ModuleBlockManager)
	}
	_, lastHeight, _ := store.GetLastCommittedBlockHeaderAndHeight(chainId)

	bm := &BlockManager{
		chainId:                  chainId,
		cache:                    cache,
		verifier:                 verifier,
		coder:                    coder,
		store:                    store,
		blockerMap:               make(map[int64]common.Blocker, defaultBlockerMapSize),
		blockerCh:                bCh,
		heightMsgCh:              hCh,
		ctx:                      ctx,
		lastCommittedBlockHeight: lastHeight,
		listeners:                make(map[int64]chan int64),
		log:                      log,
	}
	return bm
}

// Start startups BlockManager
func (bm *BlockManager) Start() {
	bm.log.Debugf("[ChainId:%s] start block manager!", bm.chainId)
	bm.receiveBlocker()
}

// RegisterListener registers listener the missing block for prover
func (bm *BlockManager) RegisterListener(height int64, ch chan int64) int64 {
	bm.Lock()
	defer bm.Unlock()
	if height <= bm.lastCommittedBlockHeight {
		return height
	}

	if height > bm.lastCommittedBlockHeight {
		bm.listeners[height] = ch
	}
	return -1
}

// RemoveListener removes listener when prover gets the missing block
func (bm *BlockManager) RemoveListener(height int64) {
	bm.Lock()
	defer bm.Unlock()
	delete(bm.listeners, height)
}

// receiveBlocker receives block from blockerCh
func (bm *BlockManager) receiveBlocker() {
	go func() {
		for {
			select {
			case blocker, ok := <-bm.blockerCh:
				if !ok {
					bm.log.Warnf("[ChainId:%s] blockerCh hash been closed!", bm.chainId)
					return
				}

				if err := bm.processBlocker(blocker); err != nil {
					bm.log.Errorf("[ChainId:%s] process block failed!", bm.chainId)
					return
				}
			case <-bm.ctx.Done():
				return
			}
		}
	}()
}

// processBlocker process block received from blockerCh
func (bm *BlockManager) processBlocker(blocker common.Blocker) error {
	// 1. put the block into blockerMap if none exists
	height := blocker.GetHeight()
	currentHeight := bm.cache.GetCurrentHeight()
	if height <= currentHeight {
		bm.log.Debugf("[ChainId:%s] expired block! current local height:%d, block height:%d",
			bm.chainId, currentHeight, height)
		return nil
	}

	if _, ok := bm.blockerMap[height]; ok {
		bm.log.Debugf("[ChainId:%s] duplicate block! block height:%d", bm.chainId, height)
		return nil
	}

	bm.blockerMap[height] = blocker
	bm.log.Debugf("[ChainId:%s] mapping the block! block height:%d", bm.chainId, blocker.GetHeight())

	// 2. commit genesis block
	if height == 0 {
		err := bm.commitBlock(blocker)
		if err != nil {
			bm.log.Errorf("[ChainId:%s] commit genesis block failed! genesis block hash:%x",
				bm.chainId, blocker.GetBlockHash())
			return err
		}

		err = bm.cache.SetCurrentHeight(height)
		if err != nil {
			bm.log.Errorf("[ChainId:%s] set currentHeight failed! current local height:%d, new height:%d",
				bm.chainId, bm.cache.GetCurrentHeight(), height)
			return err
		}
		delete(bm.blockerMap, height)

		// put height to listeners if prover has registered this height
		bm.notifyProver(height)
	}

	// 3. commit all blocks that are synchronized and can be committed
	for {
		currentHeight := bm.cache.GetCurrentHeight()
		remoteMaxHeight := bm.cache.GetRemoteMaxHeight()
		nextHeight := currentHeight + 1

		if nextHeight > remoteMaxHeight {
			bm.log.Infof("[ChainId:%s] spv has synced to the highest block! current local height:%d, remote max height:%d",
				bm.chainId, currentHeight, remoteMaxHeight)
			break
		}

		block, ok := bm.blockerMap[nextHeight]
		// missing the block to be committed
		if !ok {
			bm.log.Debugf("[ChainId:%s] missing block! block height:%d, current local height:%d",
				bm.chainId, nextHeight, currentHeight)
			return nil
		}

		delete(bm.blockerMap, nextHeight)

		err := bm.validBlock(block)
		// block is invalid
		if err != nil {
			bm.log.Debugf("[ChainId:%s] invalid block! block height:%d, err msg:%s", bm.chainId, nextHeight, err.Error())
			heightMsg := &HeightMsg{
				height:       nextHeight,
				isFromRemote: false,
			}

			bm.heightMsgCh <- heightMsg
			return nil
		}
		// block is valid
		err = bm.commitBlock(block)
		if err != nil {
			bm.log.Errorf("[ChainId:%s] commit block failed! block height:%d, block hash:%x",
				bm.chainId, block.GetHeight(), block.GetBlockHash())
			return err
		}

		// put height to listeners if prover has registered this height
		bm.notifyProver(block.GetHeight())

		if block.GetHeight()%10 == 0 {
			bm.log.Infof("[ChainId:%s] commit block successfully! block height:%d, block hash:%x",
				bm.chainId, block.GetHeight(), block.GetBlockHash())
		}
	}

	return nil
}

// validBlock verifies block Validity
func (bm *BlockManager) validBlock(blocker common.Blocker) error {
	preBlockHeaderBytes, _, ok := bm.store.GetLastCommittedBlockHeaderAndHeight(bm.chainId)
	if !ok {
		return fmt.Errorf("[ChainId:%s] get last committed block header failed", bm.chainId)
	}
	preBlockHeader, err := bm.coder.DeserializeBlockHeader(preBlockHeaderBytes)
	if err != nil {
		return err
	}
	preBlockHash := preBlockHeader.GetBlockHash()
	return bm.verifier.ValidBlock(blocker, preBlockHash)
}

// commitBlock commits block to db
func (bm *BlockManager) commitBlock(block common.Blocker) error {
	serializeBlock, err := bm.coder.SerializeBlockHeader(block.GetBlockHeader())
	// block has been modified elsewhere, request this block again.
	if err != nil {
		heightMsg := &HeightMsg{
			height:       block.GetHeight(),
			isFromRemote: false,
		}
		bm.heightMsgCh <- heightMsg
		return nil
	}

	txHashesMap, err := bm.coder.GenerateTransactionHashes(block)
	if err != nil {
		return err
	}

	err = bm.store.CommitBlockHeaderAndTxHashes(block.GetChainId(), block.GetHeight(), serializeBlock, txHashesMap)
	if err != nil {
		return err
	}

	// update the current height
	err = bm.cache.SetCurrentHeight(block.GetHeight())
	if err != nil {
		bm.log.Errorf("[ChainId:%s] set current local height failed! current local height:%d, block height:%d",
			bm.chainId, bm.cache.GetCurrentHeight(), block.GetHeight())
		return err
	}

	return nil
}

// notifyProver puts height to listeners if prover has registered this height
func (bm *BlockManager) notifyProver(height int64) {
	bm.Lock()
	defer bm.Unlock()
	bm.lastCommittedBlockHeight = height
	ch, ok := bm.listeners[height]
	if ok {
		ch <- height
		close(ch)
	}
}
