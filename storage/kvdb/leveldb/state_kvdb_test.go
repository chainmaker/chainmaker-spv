/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package leveldb

import (
	"testing"

	"chainmaker.org/chainmaker-spv/conf"
	"github.com/stretchr/testify/require"
)

func TestStateDB_Func(t *testing.T) {
	stateDB := NewKvStateDB(&conf.StoreConfig{
		Provider: "leveldb",
		LevelDB: &conf.LevelDBConfig{
			StorePath:       "../data/spv_db",
			WriteBufferSize: 4,
			BloomFilterBits: 10,
		},
	}, nil)
	chainID := "chain1"
	var height int64 = 1
	err := stateDB.CommitBlockHeaderAndTxHashes(chainID, int64(1), []byte("chain1 Header"), map[string][]byte{
		"tx1": []byte("tx1's hash"),
		"tx2": []byte("tx2's hash"),
		"tx3": []byte("tx3's hash"),
	})
	require.Nil(t, err)

	bh, ok := stateDB.GetBlockHeaderByHeight(chainID, height)
	require.NotNil(t, bh)
	require.Nil(t, err)

	bh, h, ok := stateDB.GetLastCommittedBlockHeaderAndHeight(chainID)
	require.NotNil(t, bh)
	require.Equal(t, h, int64(1))
	require.Equal(t, ok, true)

	tx, h, ok := stateDB.GetTransactionInfoByTxKey(chainID, "tx1")
	require.NotNil(t, tx)
	require.Equal(t, h, int64(1))
	require.Equal(t, ok, true)

	tx, h, ok = stateDB.GetTransactionInfoByTxKey(chainID, "tx2")
	require.NotNil(t, tx)
	require.Equal(t, h, int64(1))
	require.Equal(t, ok, true)

	tx, h, ok = stateDB.GetTransactionInfoByTxKey(chainID, "tx3")
	require.NotNil(t, bh)
	require.Equal(t, h, int64(1))
	require.Equal(t, ok, true)

	err = stateDB.WriteChainConfig(chainID, []byte("chain1_config"))
	require.Nil(t, err)

	cc, ok := stateDB.GetChainConfig(chainID)
	require.NotNil(t, cc)
	require.Nil(t, err)

	stateDB.Close()
}
