/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package leveldb is leveldb module
package leveldb

import (
	"fmt"
	"strconv"

	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/logger"
	"chainmaker.org/chainmaker-spv/storage/kvdb"
	"go.uber.org/zap"
)

const (
	BlockHeaderPrefix              = "BH/%s"  // BH/chainId+height
	TxHashPrefix                   = "THA/%s" // THA/chainId+txKey
	TxHeightPrefix                 = "THE/%s" // THE/chainId+txKey
	LastCommittedBlockHeaderPrefix = "LB/%s"  // LB/chainId
	LastCommittedBlockHeightPrefix = "LH/%s"  // LH/chainId
	ChainConfigPrefix              = "CC/%s"  // CC/chainId
)

// KvStateDB is the implementation of StateDB interface
type KvStateDB struct {
	provider kvdb.KvDBProvider
	log      *zap.SugaredLogger
}

// NewKvStateDB creates KvStateDB
func NewKvStateDB(storeConfig *conf.StoreConfig, log *zap.SugaredLogger) *KvStateDB {
	if log == nil {
		log = logger.GetLogger(logger.ModuleStateDB)
	}

	kvStateDB := &KvStateDB{
		provider: NewLevelDBProvider(storeConfig.LevelDB),
		log:      log,
	}
	return kvStateDB
}

// CommitBlockHeaderAndTxHashes commits the serialized block header and transaction hash in an atomic operation
func (db *KvStateDB) CommitBlockHeaderAndTxHashes(chainId string, height int64, serializedHeader []byte,
	txHashes map[string][]byte) error {
	batch := kvdb.NewKvDBBatcher()
	blockHeaderKey := blockHeaderKey(chainId, strconv.FormatInt(height, 10))
	batch.Add(blockHeaderKey, serializedHeader)
	for txKey, txHash := range txHashes {
		txHashKey := txHashKey(chainId, txKey)
		txHeightKey := txHeightKey(chainId, txKey)
		batch.Add(txHashKey, txHash)
		batch.Add(txHeightKey, []byte(strconv.FormatInt(height, 10)))
	}

	batch.Add(lastCommittedBlockHeaderKey(chainId), serializedHeader)
	batch.Add(lastCommittedBlockHeightKey(chainId), []byte(strconv.FormatInt(height, 10)))

	err := db.provider.WriteBatch(batch)
	if err != nil {
		db.log.Errorf("commit block header failed, block height:%d", height)
		return err
	}
	db.log.Debugf("commit block header successfully, block height:%d", height)
	return nil
}

// GetBlockHeaderByHeight returns the serialized block header given it's height, or returns nil if none exists
func (db *KvStateDB) GetBlockHeaderByHeight(chainId string, height int64) ([]byte, bool) {
	blockHeaderKey := blockHeaderKey(chainId, strconv.FormatInt(height, 10))
	header, ok := db.provider.Get(blockHeaderKey)
	if !ok {
		db.log.Warnf("get block header failed, block height:%d", height)
		return nil, false
	}

	return header, true
}

// GetLastCommittedBlockHeaderAndHeight returns the last committed block header,or returns nil if none exists
func (db *KvStateDB) GetLastCommittedBlockHeaderAndHeight(chainId string) ([]byte, int64, bool) {
	lastCommittedBlockHeader, ok := db.provider.Get(lastCommittedBlockHeaderKey(chainId))
	if !ok {
		db.log.Debugf("get last committed block header failed")
		return nil, -1, false
	}

	lastCommittedBlockHeight, ok := db.provider.Get(lastCommittedBlockHeightKey(chainId))
	if !ok {
		db.log.Warnf("get last committed block height failed")
		return nil, -1, false
	}
	height, err := strconv.ParseInt(string(lastCommittedBlockHeight), 10, 64)
	if err != nil {
		return nil, -1, false
	}

	return lastCommittedBlockHeader, height, true
}

// GetTransactionInfoByTxKey returns transaction hash and block height by txId or txHash, or returns nil if none exists
func (db *KvStateDB) GetTransactionInfoByTxKey(chainId string, txKey string) ([]byte, int64, bool) {
	txHashKey := txHashKey(chainId, txKey)
	txHeightKey := txHeightKey(chainId, txKey)
	txHash, ok := db.provider.Get(txHashKey)
	if !ok {
		db.log.Warnf("get transaction hash failed, transaction key:%s", txKey)
		return nil, -1, false
	}

	txHeight, ok := db.provider.Get(txHeightKey)
	if !ok {
		db.log.Warnf("get transaction height failed, transaction key:%s", txKey)
		return nil, -1, false
	}
	height, err := strconv.ParseInt(string(txHeight), 10, 64)
	if err != nil {
		return nil, -1, false
	}

	return txHash, height, true
}

// WriteChainConfig put the chain config to db
func (db *KvStateDB) WriteChainConfig(chainId string, chainConfig []byte) error {
	err := db.provider.Put(chainConfigKey(chainId), chainConfig)
	if err != nil {
		db.log.Warnf("write chain conf failed")
		return err
	}

	db.log.Debugf("write chain conf successfully")
	return nil
}

// GetChainConfig returns chain config, or returns nil if none exists
func (db *KvStateDB) GetChainConfig(chainId string) ([]byte, bool) {
	cc, ok := db.provider.Get(chainConfigKey(chainId))
	if !ok {
		db.log.Warnf("get chain conf failed")
		return nil, false
	}

	return cc, true
}

// Close is used to close database
func (db *KvStateDB) Close() {
	db.provider.Close()
}

func blockHeaderKey(chainId string, height string) string {
	return fmt.Sprintf(BlockHeaderPrefix, chainId+height)
}

func txHashKey(chainId string, txKey string) string {
	return fmt.Sprintf(TxHashPrefix, chainId+txKey)
}

func txHeightKey(chainId string, txKey string) string {
	return fmt.Sprintf(TxHeightPrefix, chainId+txKey)
}

func lastCommittedBlockHeaderKey(chainId string) string {
	return fmt.Sprintf(LastCommittedBlockHeaderPrefix, chainId)
}

func lastCommittedBlockHeightKey(chainId string) string {
	return fmt.Sprintf(LastCommittedBlockHeightPrefix, chainId)
}

func chainConfigKey(chainId string) string {
	return fmt.Sprintf(ChainConfigPrefix, chainId)
}
