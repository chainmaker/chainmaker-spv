/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package leveldb

import (
	"testing"

	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/storage/kvdb"
	"github.com/stretchr/testify/require"
)

func TestLevelDBProvider_Func(t *testing.T) {
	provider := NewLevelDBProvider(&conf.LevelDBConfig{
		StorePath:       "../data/spv_db",
		WriteBufferSize: 4,
		BloomFilterBits: 10,
	})
	key := "hello"
	val := []byte("word")
	err := provider.Put(key, val)
	require.Nil(t, err)

	has, err := provider.Has(key)
	require.Equal(t, has, true)
	require.Nil(t, err)

	v, ok := provider.Get(key)
	require.Equal(t, string(v), "word")
	require.Equal(t, ok, true)

	err = provider.Delete(key)
	require.Nil(t, err)

	has, err = provider.Has(key)
	require.Equal(t, has, false)
	require.Nil(t, err)

	v, ok = provider.Get(key)
	require.Nil(t, v)
	require.Equal(t, ok, false)

	batcher := kvdb.NewKvDBBatcher()
	batcher.Add("k1", []byte("k1's value"))
	batcher.Add("k2", []byte("k2's value"))
	batcher.Add("k3", []byte("k3's value"))

	err = provider.WriteBatch(batcher)
	require.Nil(t, err)

	provider.Close()
}
