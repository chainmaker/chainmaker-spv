/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package kvdb is kvdb module
package kvdb

// KvDBProvider defines kv db provider interface
type KvDBProvider interface {
	// Get returns value by key
	Get(key string) ([]byte, bool)

	// Put saves the key-values
	Put(key string, value []byte) error

	// Has return true if the given key exist, or return false if none exists
	Has(key string) (bool, error)

	// Delete deletes the given key
	Delete(key string) error

	// WriteBatch writes a batch in an atomic operation
	WriteBatch(batch *KvDBBatcher) error

	Close()
}

type KvDBBatcher struct {
	kvs []*Kv
}

func NewKvDBBatcher() *KvDBBatcher {
	return &KvDBBatcher{
		kvs: make([]*Kv, 0),
	}
}

func (b *KvDBBatcher) Add(key string, value []byte) {
	b.kvs = append(b.kvs, NewKv(key, value))
}

func (b *KvDBBatcher) GetKvs() []*Kv {
	return b.kvs
}

func (b *KvDBBatcher) Len() int {
	return len(b.kvs)
}

type Kv struct {
	key   string
	value []byte
}

func NewKv(key string, value []byte) *Kv {
	return &Kv{
		key:   key,
		value: value,
	}
}

func (kv *Kv) GetKey() string {
	return kv.key
}

func (kv *Kv) GetValue() []byte {
	return kv.value
}
