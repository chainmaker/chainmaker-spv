/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package storage is storage module of all chains
package storage

// StateDB defines storage interface
type StateDB interface {
	// CommitBlockHeaderAndTxHashes commits the serialized block header and transaction hash in an atomic operation
	CommitBlockHeaderAndTxHashes(chainId string, height int64, serializedHeader []byte, txHashes map[string][]byte) error

	// GetBlockHeaderByHeight returns the serialized block header given it's height, or returns nil if none exists
	GetBlockHeaderByHeight(chainId string, height int64) ([]byte, bool)

	// GetLastCommittedBlockHeaderAndHeight returns the last committed block header,or returns nil if none exists
	GetLastCommittedBlockHeaderAndHeight(chainId string) ([]byte, int64, bool)

	// GetTransactionInfoByTxKey returns transaction hash and block height by txId or txHash, or returns nil if none exists
	GetTransactionInfoByTxKey(chainId string, txKey string) ([]byte, int64, bool)

	// WriteChainConfig put the chain config to db
	WriteChainConfig(chainId string, chainConfig []byte) error

	// GetChainConfig returns chain config, or returns nil if none exists
	GetChainConfig(chainId string) ([]byte, bool)

	// Close is used to close database
	Close()
}
