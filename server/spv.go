/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package server

import (
	"chainmaker.org/chainmaker-spv/adapter"
	"chainmaker.org/chainmaker-spv/coder"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/manager"
	"chainmaker.org/chainmaker-spv/prover"
	"chainmaker.org/chainmaker-spv/storage"
	"chainmaker.org/chainmaker-spv/verifier"
	"go.uber.org/zap"
)

// SPV is a spv instance for a specific remote chain
type SPV struct {
	adapter      adapter.SDKAdapter
	verifier     verifier.Verifier
	coder        coder.Coder
	stateDB      storage.StateDB
	stateManager *manager.StateManager
	prover       prover.Prover
}

// NewSPV creates a spv for a specific remote chain
func NewSPV(chain *conf.ChainConfig, store storage.StateDB, log *zap.SugaredLogger) (*SPV, error) {
	cmSDKAdapter, err := adapter.NewChainMakerSDKAdapter(chain.ChainId, chain.SDKConfigPath, log)
	if err != nil {
		return nil, err
	}
	cmVerifier := verifier.NewChainMakerVerifier()
	cmCoder := coder.NewChainMakerCoder()
	cmStateManager := manager.NewStateManager(chain.ChainId, cmSDKAdapter, cmVerifier, cmCoder, store, log)

	cmProver := prover.NewChainMakerProver(chain.ChainId, cmSDKAdapter, cmCoder, store,
		cmStateManager.GetBlockManager(), log)

	spv := &SPV{
		adapter:      cmSDKAdapter,
		verifier:     cmVerifier,
		coder:        cmCoder,
		stateDB:      store,
		stateManager: cmStateManager,
		prover:       cmProver,
	}

	return spv, nil
}

// Start startups a spv instance
func (s *SPV) Start() error {
	err := s.stateManager.Start()
	if err != nil {
		return err
	}
	return nil
}

// Stop stops a spv instance
func (s *SPV) Stop() error {
	err := s.stateManager.Stop()
	if err != nil {
		return err
	}
	return nil
}

// GetAdapter gets adapter
func (s *SPV) GetAdapter() adapter.SDKAdapter {
	return s.adapter
}

// GetVerifier gets verifier
func (s *SPV) GetVerifier() verifier.Verifier {
	return s.verifier
}

// GetCoder gets coder
func (s *SPV) GetCoder() coder.Coder {
	return s.coder
}

// GetStateDB gets stateDB
func (s *SPV) GetStateDB() storage.StateDB {
	return s.stateDB
}

// GetStateManager gets state manager
func (s *SPV) GetStateManager() *manager.StateManager {
	return s.stateManager
}

// GetProver gets prover
func (s *SPV) GetProver() prover.Prover {
	return s.prover
}
