/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package server

import (
	"testing"

	"chainmaker.org/chainmaker-spv/coder"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/manager"
	"chainmaker.org/chainmaker-spv/mock"
	"chainmaker.org/chainmaker-spv/pb/protogo"
	"chainmaker.org/chainmaker-spv/prover"
	"chainmaker.org/chainmaker-spv/storage/kvdb/leveldb"
	"chainmaker.org/chainmaker-spv/verifier"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestSPV_Func(t *testing.T) {
	cc := &conf.ChainConfig{
		ChainId:                    "chain1",
		SyncChainInfoIntervalMills: 1000,
		SDKConfigPath:              "../config/chainmaker_sdk_config_chain1.yml",
	}
	conf.SPVConfig = &conf.Config{
		Chains: []*conf.ChainConfig{
			cc,
		},
		StorageConfig: &conf.StoreConfig{
			Provider: "leveldb",
			LevelDB: &conf.LevelDBConfig{
				StorePath:       "../data/spv_db",
				WriteBufferSize: 4,
				BloomFilterBits: 10,
			},
		},
	}
	cmStore := leveldb.NewKvStateDB(conf.SPVConfig.StorageConfig, nil)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	sdkAdapter := mock.NewMockSDKAdapter(ctrl)
	sdkAdapter.EXPECT().GetChainConfig().Return(&protogo.ChainConfig{
		ChainId:  "chain1",
		HashType: "HASH256",
	}, nil)
	sdkAdapter.EXPECT().SubscribeBlock().Return(make(chan common.Blocker), nil)
	sdkAdapter.EXPECT().Stop().Return(nil)

	ver := verifier.NewChainMakerVerifier()
	cdr := coder.NewChainMakerCoder()
	cmStateManager := manager.NewStateManager(conf.SPVConfig.Chains[0].ChainId, sdkAdapter, ver, cdr, cmStore, nil)
	proverIns := prover.NewChainMakerProver(conf.SPVConfig.Chains[0].ChainId, sdkAdapter, cdr, cmStore, cmStateManager.GetBlockManager(), nil)
	spvIns := &SPV{
		adapter:      sdkAdapter,
		verifier:     ver,
		coder:        cdr,
		stateDB:      cmStore,
		stateManager: cmStateManager,
		prover:       proverIns,
	}
	err := spvIns.Start()
	require.Nil(t, err)
	defer spvIns.Stop()

	aptr := spvIns.GetAdapter()
	require.NotNil(t, aptr)

	cdr2 := spvIns.GetCoder()
	require.NotNil(t, cdr2)

	pvr := spvIns.GetProver()
	require.NotNil(t, pvr)

	db := spvIns.GetStateDB()
	require.NotNil(t, db)

	vfer := spvIns.GetVerifier()
	require.NotNil(t, vfer)

	smr := spvIns.GetStateManager()
	require.NotNil(t, smr)
}
