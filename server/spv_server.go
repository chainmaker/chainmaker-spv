/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package server is SPV module
package server

import (
	"fmt"
	"time"

	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/logger"
	"chainmaker.org/chainmaker-spv/pb/api"
	"chainmaker.org/chainmaker-spv/storage"
	"chainmaker.org/chainmaker-spv/storage/kvdb/leveldb"
	"go.uber.org/zap"
)

// SPVServer contains all chains' SPV
type SPVServer struct {
	stateDB storage.StateDB
	spvs    map[string]*SPV // map[chainId]=*SPV
	log     *zap.SugaredLogger
}

// NewSPVServer inits spv config and creates spv server when integrated as a component
func NewSPVServer(ymFile string, log *zap.SugaredLogger) (*SPVServer, error) {
	var server = &SPVServer{
		spvs: make(map[string]*SPV),
	}
	// 1. init spv config
	err := conf.InitSPVConfigWithYmlFile(ymFile)
	if err != nil {
		return nil, err
	}

	if log == nil {
		log = logger.GetDefaultLogger()
	}

	// 2. new store module
	store := leveldb.NewKvStateDB(conf.SPVConfig.StorageConfig, log)
	for _, chain := range conf.SPVConfig.Chains {
		// 3. new spv
		spv, err := NewSPV(chain, store, log)
		if err != nil {
			return nil, err
		}
		server.spvs[chain.ChainId] = spv
	}

	server.stateDB = store
	server.log = log

	// 4. return SPVServer
	return server, nil
}

// IntSPVServer inits spv server when deployed independently
func IntSPVServer() (*SPVServer, error) {
	var server = &SPVServer{
		spvs: make(map[string]*SPV),
	}

	// 1. new store module
	store := leveldb.NewKvStateDB(conf.SPVConfig.StorageConfig, nil)
	for _, chain := range conf.SPVConfig.Chains {
		// 2. new spv
		spv, err := NewSPV(chain, store, nil)
		if err != nil {
			return nil, err
		}
		server.spvs[chain.ChainId] = spv
	}

	server.stateDB = store
	server.log = logger.GetLogger(logger.ModuleSpv)

	// 3. return SPVServer
	return server, nil
}

// Start startups all chains' spv
func (ss *SPVServer) Start() error {
	ss.log.Infof("Start SPV Server!")
	for _, spv := range ss.spvs {
		if err := spv.Start(); err != nil {
			return err
		}
	}
	return nil
}

// ValidTransaction verifies the existence and validity of the transaction
func (ss *SPVServer) ValidTransaction(info *api.TxValidationInfo, timeout time.Duration) error {
	if info == nil {
		return fmt.Errorf("TxValidationInfo is nil")
	}

	spv, ok := ss.spvs[info.ChainId]
	if !ok {
		return fmt.Errorf("chain id:%s is invalid", info.ChainId)
	}

	err := spv.GetProver().ValidTransaction(info, timeout)
	if err != nil {
		return err
	}

	return nil
}

// Stop stops all chains' spv
func (ss *SPVServer) Stop() error {
	for _, spv := range ss.spvs {
		if err := spv.Stop(); err != nil {
			return err
		}
	}
	ss.stateDB.Close()
	ss.log.Infof("Stop SPV Server!")

	return nil
}
