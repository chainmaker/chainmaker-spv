/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package server

import (
	"testing"
	"time"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/coder"
	"chainmaker.org/chainmaker-spv/common"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/manager"
	"chainmaker.org/chainmaker-spv/mock"
	"chainmaker.org/chainmaker-spv/pb/api"
	"chainmaker.org/chainmaker-spv/pb/protogo"
	"chainmaker.org/chainmaker-spv/prover"
	"chainmaker.org/chainmaker-spv/storage"
	"chainmaker.org/chainmaker-spv/verifier"
	"github.com/gogo/protobuf/proto"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func Test_IntSPVServer(t *testing.T) {
	conf.SPVConfig.StorageConfig = &conf.StoreConfig{
		Provider: "leveldb",
		LevelDB: &conf.LevelDBConfig{
			StorePath:       "../data/spv_db",
			WriteBufferSize: 4,
			BloomFilterBits: 10,
		},
	}

	srv, err := IntSPVServer()
	require.NoError(t, err)
	require.NotNil(t, srv)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	//conf.SPVConfig.StorageConfig.LevelDB.StorePath = "../data/spv_db2"
	srv.spvs["chain1"] = newTestSpv(ctrl, srv.stateDB)
	srv.Start()
	defer srv.Stop()
	conf.BlockChainConfigs["chain1"] = &protogo.ChainConfig{
		HashType: "SHA256",
	}

	param := &api.KVPair{
		Key:   "key1",
		Value: "value1",
	}
	err = srv.ValidTransaction(&api.TxValidationInfo{
		ChainId:     "chain1",
		TxKey:       "tx1",
		BlockHeight: 2,
		ContractData: &api.ContractData{
			ContractName: "contract1",
			Method:       "unknown",
			Params: []*api.KVPair{
				param,
			},
		},
	}, time.Duration(5)*time.Second)
	require.Nil(t, err)

}

func newTestSpv(ctrl *gomock.Controller, cmStore storage.StateDB) *SPV {
	chainID := "chain1"
	cc := &conf.ChainConfig{
		ChainId:                    chainID,
		SyncChainInfoIntervalMills: 1000,
		SDKConfigPath:              "../config/chainmaker_sdk_config_chain1.yml",
	}
	conf.SPVConfig.Chains = []*conf.ChainConfig{
		cc,
	}

	sdkAdapter := mock.NewMockSDKAdapter(ctrl)
	sdkAdapter.EXPECT().GetChainConfig().Return(&protogo.ChainConfig{
		ChainId:  "chain1",
		HashType: "HASH256",
	}, nil)
	sdkAdapter.EXPECT().SubscribeBlock().Return(make(chan common.Blocker), nil)
	sdkAdapter.EXPECT().Stop().Return(nil)

	param := &cmCommonPb.KeyValuePair{
		Key:   "key1",
		Value: "value1",
	}
	queryOrTransactPayload := cmCommonPb.QueryPayload{
		ContractName: "contract1",
		Method:       "unknown",
		Parameters: []*cmCommonPb.KeyValuePair{
			param,
		},
	}
	pl, _ := proto.Marshal(&queryOrTransactPayload)
	sdkAdapter.EXPECT().GetTransactionByTxKey(gomock.Eq("tx1")).Return(&common.CMTransaction{
		Transaction: &cmCommonPb.Transaction{
			Header: &cmCommonPb.TxHeader{
				ChainId: "chain1",
			},
			RequestPayload: pl,
		},
	}, nil).AnyTimes()
	sdkAdapter.EXPECT().GetChainHeight().Return(int64(1), nil).AnyTimes()

	cmStore.CommitBlockHeaderAndTxHashes(chainID, 2, []byte("chain1 Header"), map[string][]byte{
		"tx1": {109, 36, 223, 137, 153, 170, 131, 73, 193, 83, 30, 218, 1, 113, 187, 78, 97, 154, 219, 45, 89, 196, 197, 157, 20, 243, 67, 80, 103, 152, 42, 170},
		"tx2": []byte("tx2's hash"),
		"tx3": []byte("tx3's hash"),
	})
	ver := verifier.NewChainMakerVerifier()
	cdr := coder.NewChainMakerCoder()
	cmStateManager := manager.NewStateManager(conf.SPVConfig.Chains[0].ChainId, sdkAdapter, ver, cdr, cmStore, nil)
	proverIns := prover.NewChainMakerProver(conf.SPVConfig.Chains[0].ChainId, sdkAdapter, cdr, cmStore, cmStateManager.GetBlockManager(), nil)
	return &SPV{
		adapter:      sdkAdapter,
		verifier:     ver,
		coder:        cdr,
		stateDB:      cmStore,
		stateManager: cmStateManager,
		prover:       proverIns,
	}
}
