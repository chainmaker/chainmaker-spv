/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package common

import (
	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/utils"
)

// CMTransaction packages ChainMaker's Transaction
type CMTransaction struct {
	Transaction *cmCommonPb.Transaction
}

// GetStatusCode returns the transaction status code
func (tx *CMTransaction) GetStatusCode() int32 {
	return int32(tx.Transaction.GetResult().GetCode())
}

// GetTransactionHash returns transaction hash
func (tx *CMTransaction) GetTransactionHash() ([]byte, error) {
	return utils.CalcTxHash(tx.Transaction)
}

// GetContractName returns the contract name of transaction
func (tx *CMTransaction) GetContractName() (string, error) {
	contractInfo, err := utils.GetContractInfoByTxPayload(tx.Transaction)
	if err != nil {
		return "", err
	}

	return contractInfo.ContractName, nil
}

// GetMethod returns the method in contract method of transaction
func (tx *CMTransaction) GetMethod() (string, error) {
	contractInfo, err := utils.GetContractInfoByTxPayload(tx.Transaction)
	if err != nil {
		return "", err
	}

	return contractInfo.Method, nil
}

// GetParams returns parameters of transaction
func (tx *CMTransaction) GetParams() ([]interface{}, error) {
	contractInfo, err := utils.GetContractInfoByTxPayload(tx.Transaction)
	if err != nil {
		return nil, err
	}

	return contractInfo.Parameters, nil
}
