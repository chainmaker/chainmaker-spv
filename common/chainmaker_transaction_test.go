/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package common

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/pb/protogo"
	"github.com/gogo/protobuf/proto"
	"github.com/stretchr/testify/require"
)

func TestCMTransaction_Func(t *testing.T) {
	conf.BlockChainConfigs["chain1"] = &protogo.ChainConfig{
		ChainId:  "chain1",
		HashType: "SHA256",
	}
	requestPayload := newRequestPayload()
	require.NotNil(t, requestPayload)

	tran := &CMTransaction{
		Transaction: &cmCommonPb.Transaction{
			Header: &cmCommonPb.TxHeader{
				ChainId: "chain1",
			},
			RequestPayload: requestPayload,
			Result: &cmCommonPb.Result{
				Code:           cmCommonPb.TxStatusCode_SUCCESS,
				ContractResult: &cmCommonPb.ContractResult{},
			},
		},
	}
	cn, err := tran.GetContractName()
	require.Equal(t, cn, "contractName")
	require.Nil(t, err)

	method, err := tran.GetMethod()
	require.Equal(t, method, "method")
	require.Nil(t, err)

	params, err := tran.GetParams()
	require.NotNil(t, params)
	require.Nil(t, err)

	code := tran.GetStatusCode()
	require.Equal(t, code, int32(cmCommonPb.TxStatusCode_SUCCESS))

	hash, err := tran.GetTransactionHash()
	require.NotNil(t, hash)
	require.Nil(t, err)

}

func newRequestPayload() []byte {
	param := &cmCommonPb.KeyValuePair{
		Key:   "key",
		Value: "value",
	}
	rp := &cmCommonPb.TransactPayload{
		ContractName: "contractName",
		Method:       "method",
		Parameters: []*cmCommonPb.KeyValuePair{
			param,
		},
	}
	rpBytes, err := proto.Marshal(rp)
	if err != nil {
		return nil
	}

	return rpBytes
}
