/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package common

import cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"

// CMBlock packages ChainMaker's block
type CMBlock struct {
	Block *cmCommonPb.Block
}

// GetBlockHeader returns block header
func (cmb *CMBlock) GetBlockHeader() Header {
	cmHeader := &CMBlockHeader{
		BlockHeader: cmb.Block.GetHeader(),
	}
	return cmHeader
}

// GetChainId returns chainId
func (cmb *CMBlock) GetChainId() string {
	return cmb.Block.GetHeader().GetChainId()
}

// GetPreHash returns the hash value of previous block
func (cmb *CMBlock) GetPreHash() []byte {
	return cmb.Block.GetHeader().GetPreBlockHash()
}

// GetTxRoot returns the root hash of transaction tree
func (cmb *CMBlock) GetTxRoot() []byte {
	return cmb.Block.GetHeader().GetTxRoot()
}

// GetHeight returns block height
func (cmb *CMBlock) GetHeight() int64 {
	return cmb.Block.GetHeader().GetBlockHeight()
}

// GetBlockHash returns block hash
func (cmb *CMBlock) GetBlockHash() []byte {
	return cmb.Block.GetHeader().GetBlockHash()
}

// CMBlockHeader packages ChainMaker's block header
type CMBlockHeader struct {
	BlockHeader *cmCommonPb.BlockHeader
}

// GetChainId returns chainId
func (cmh *CMBlockHeader) GetChainId() string {
	return cmh.BlockHeader.GetChainId()
}

// GetPreHash returns the hash value of previous block
func (cmh *CMBlockHeader) GetPreHash() []byte {
	return cmh.BlockHeader.GetPreBlockHash()
}

// GetTxRoot returns the root hash of transaction tree
func (cmh *CMBlockHeader) GetTxRoot() []byte {
	return cmh.BlockHeader.GetTxRoot()
}

// GetHeight returns block height
func (cmh *CMBlockHeader) GetHeight() int64 {
	return cmh.BlockHeader.GetBlockHeight()
}

// GetBlockHash returns block hash
func (cmh *CMBlockHeader) GetBlockHash() []byte {
	return cmh.BlockHeader.GetBlockHash()
}
