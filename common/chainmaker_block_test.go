/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package common

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"github.com/stretchr/testify/require"
)

func TestCMBlock_Func(t *testing.T) {
	block := CMBlock{
		Block: &cmCommonPb.Block{
			Header: &cmCommonPb.BlockHeader{
				ChainId:      "chain1",
				PreBlockHash: []byte("preHash"),
				TxRoot:       []byte("txRoot"),
				BlockHeight:  int64(1),
				BlockHash:    []byte("blockHash"),
			},
		},
	}
	header := block.GetBlockHeader()
	require.NotNil(t, header)

	chainId := block.GetChainId()
	require.Equal(t, chainId, "chain1")

	preHash := block.GetPreHash()
	require.Equal(t, string(preHash), "preHash")

	txRoot := block.GetTxRoot()
	require.Equal(t, string(txRoot), "txRoot")

	height := block.GetHeight()
	require.Equal(t, height, int64(1))

	blockHash := block.GetBlockHash()
	require.Equal(t, string(blockHash), "blockHash")
}

func TestCMBlockHeader_Func(t *testing.T) {
	header := &CMBlockHeader{
		BlockHeader: &cmCommonPb.BlockHeader{
			ChainId:      "chain1",
			PreBlockHash: []byte("preHash"),
			TxRoot:       []byte("txRoot"),
			BlockHeight:  int64(1),
			BlockHash:    []byte("blockHash"),
		},
	}
	chainId := header.GetChainId()
	require.Equal(t, chainId, "chain1")

	preHash := header.GetPreHash()
	require.Equal(t, string(preHash), "preHash")

	txRoot := header.GetTxRoot()
	require.Equal(t, string(txRoot), "txRoot")

	height := header.GetHeight()
	require.Equal(t, height, int64(1))

	blockHash := header.GetBlockHash()
	require.Equal(t, string(blockHash), "blockHash")
}
