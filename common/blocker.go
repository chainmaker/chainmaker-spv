/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package common defines the interfaces that block, block header and transaction need to implement,
// and contains the implementations of each chain
package common

// Blocker defines the interface that the block needs to implement
type Blocker interface {
	Header

	// GetBlockHeader returns block header
	GetBlockHeader() Header
}

// Header defines the interface that the block header needs to implement
type Header interface {
	// GetChainId returns chainId
	GetChainId() string

	// GetPreHash returns the hash value of previous block
	GetPreHash() []byte

	// GetTxRoot returns the root hash of transaction tree
	GetTxRoot() []byte

	// GetHeight returns block height
	GetHeight() int64

	// GetBlockHash returns block hash
	GetBlockHash() []byte
}
