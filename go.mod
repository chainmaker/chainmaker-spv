module chainmaker.org/chainmaker-spv

go 1.15

require (
	chainmaker.org/chainmaker-go/common v0.0.0
	chainmaker.org/chainmaker-sdk-go v0.0.0-00010101000000-000000000000
	github.com/Rican7/retry v0.1.0
	github.com/btcsuite/goleveldb v1.0.0
	github.com/ethereum/go-ethereum v1.10.2
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.3.1
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/spf13/cobra v1.1.3
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.16.0
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9
	google.golang.org/grpc v1.36.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)

replace (
	chainmaker.org/chainmaker-go/common => ./recourse/chainmaker-sdk/common
	chainmaker.org/chainmaker-sdk-go => ./recourse/chainmaker-sdk
)
