/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package main implements a test tool
package main

import (
	"context"
	"encoding/hex"
	"flag"
	"fmt"
	"io/ioutil"
	"math/big"
	"math/rand"
	"path/filepath"
	"strconv"
	"strings"

	"chainmaker.org/chainmaker-go/common/evmutils"
	"chainmaker.org/chainmaker-go/common/json"
	sdk "chainmaker.org/chainmaker-sdk-go"
	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/pb/api"
	"chainmaker.org/chainmaker-spv/utils"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

const (
	// sdk需要配置的内容
	chainId     = "chain1"
	orgId       = "wx-org1.chainmaker.org"
	nodeAddress = "127.0.0.1:12301"
	sdkYmlFile  = certPathPrefix + "/chainmaker_sdk_config_chain1.yml"

	// 此内容不需要修改
	certPathPrefix        = "../config"
	certPathFormat        = "/crypto-config/%s/ca"
	adminKeyPath          = certPathPrefix + "/crypto-config/%s/user/admin1/admin1.tls.key"
	adminCrtPath          = certPathPrefix + "/crypto-config/%s/user/admin1/admin1.tls.crt"
	tlsHostName           = "chainmaker.org"
	createContractTimeout = 10
)

var caPaths = []string{
	certPathPrefix + fmt.Sprintf(certPathFormat, orgId),
}

var (
	// 创建合约需要参数
	grpcAP     = "127.0.0.1:12308"
	txInfoPath = "../config/params.yml"
	abiPath    = "../contract/ledger_balance.abi"
	runTime    = int(cmCommonPb.RuntimeType_GASM)
	height     = 0
	address    = "11111"
	balance    = 0
)

func main() {
	var (
		step int
	)
	flag.IntVar(&step, "step", 1, "0: deploy wasm contract, 1: invoke wasm contract, "+
		"2: deploy evm contract, 3: invoke evm contract, 4: get transaction info, 5: verify transaction, 6: update block config")
	flag.StringVar(&grpcAP, "addr_port", grpcAP, "grpc address/port, default use 127.0.0.1:12308")
	flag.StringVar(&txInfoPath, "tx_info_path", txInfoPath, "txInfo path, default use ../config/params.yml")
	flag.StringVar(&abiPath, "abi_path", abiPath, "evm abi path, default use ../contract/ledger_balance.abi")
	flag.StringVar(&address, "addr", address, "user address , default use addr")
	flag.IntVar(&balance, "balance", balance, "balance value, default use 0")
	flag.IntVar(&runTime, "run_time", runTime, "run time type, default use RuntimeType_GASM")
	flag.IntVar(&height, "height", height, "transaction block height, default use 0")
	flag.Parse()

	switch step {
	case 0: //部署GASM合约（部署合约的交易类型为：TxType_MANAGE_USER_CONTRACT）            ./main -step 0
		createGASMContract()
	case 1: //调用GASM合约，发送交易（调用用户合约的交易类型：TxType_INVOKE_USER_CONTRACT）  ./main -step 1
		sendGASMTx()
	case 2: //部署EVM合约                                                             ./main -step 2
		createEVMContract()
	case 3: //调用EVM合约，发送交易                                                     ./main -step 3 -addr="" -balance=x
		sendEVMTx()
	case 4: //获取某一高度区块中第一笔交易的TxId                                          ./main -step 4 -height=h
		getTxInfo()
	case 5: //验证GASM合约交易有效性                                ./main -step 5 -addr_port="127.0.0.1:12308" -tx_info_path="../config/params.yml" -run_time=4
		//验证EVM合约交易有效性                                     ./main -step 5 -addr_port="127.0.0.1:12308" -tx_info_path="../config/params.yml" -run_time=5 -abi_path="../contract/ledger_balance.abi"
		verifyTransaction()
	case 6: //发送更新区块交易（更新链配置的交易类型：TxType_UPDATE_CHAIN_CONFIG）         ./main -step 6
		blockUpdate()
	default:
		panic("step is not exit!")
	}
}

func createGASMContract() {
	// create chainmaker sdk client
	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(sdkYmlFile),
	)
	fmt.Printf("sdkYmlFile:%s\n", sdkYmlFile)
	if err != nil {
		fmt.Printf("new ChainClient failed, err:%v\n", err)
		return
	}

	// create node
	node := createNode(nodeAddress, 5)

	// create admin client
	adminClient, err := sdk.NewChainClient(
		sdk.WithChainClientOrgId(orgId),
		sdk.WithChainClientChainId(chainId),
		sdk.WithUserKeyFilePath(fmt.Sprintf(adminKeyPath, orgId)),
		sdk.WithUserCrtFilePath(fmt.Sprintf(adminCrtPath, orgId)),
		sdk.AddChainClientNodeConfig(node),
	)
	if err != nil {
		fmt.Printf("new adminClient failed, err:%v\n", err)
		return
	}

	fmt.Println("====================== 创建GASM合约 ======================")
	contractName := "BalanceStable"
	version := "1.0.1"
	byteCodePath, _ := filepath.Abs("../contract/balance.wasm")
	payloadBytes, err := cc.CreateContractCreatePayload(contractName, version, byteCodePath,
		cmCommonPb.RuntimeType_GASM, []*cmCommonPb.KeyValuePair{})
	if err != nil {
		fmt.Printf("CreateContractCreatePayload failed, err:%v\n", err)
		return
	}

	// 各组织Admin权限用户签名
	signedPayloadBytes1, err := adminClient.SignContractManagePayload(payloadBytes)
	if err != nil {
		fmt.Printf("SignContractManagePayload failed, err:%v\n", err)
		return
	}

	// 收集并合并签名
	mergeSignedPayloadBytes, err := cc.MergeContractManageSignedPayload([][]byte{signedPayloadBytes1})
	if err != nil {
		fmt.Printf("MergeContractManageSignedPayload failed, err:%v\n", err)
		return
	}

	// 发送创建合约请求
	resp, err := cc.SendContractManageRequest(mergeSignedPayloadBytes, createContractTimeout, true)
	if err != nil {
		fmt.Printf("SendContractManageRequest failed, err:%v\n", err)
		return
	}

	respStr, err := json.Marshal(resp)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v\n", err)
		return
	}
	fmt.Printf("%s", respStr)
}

func createNode(nodeAddr string, connCnt int) *sdk.NodeConfig {
	node := sdk.NewNodeConfig(
		// 节点地址，格式：127.0.0.1:12301
		sdk.WithNodeAddr(nodeAddr),
		// 节点连接数
		sdk.WithNodeConnCnt(connCnt),
		// 节点是否启用TLS认证
		sdk.WithNodeUseTLS(true),
		// 根证书路径，支持多个
		sdk.WithNodeCAPaths(caPaths),
		// TLS Hostname
		sdk.WithNodeTLSHostName(tlsHostName),
	)

	return node
}

func sendGASMTx() {
	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(sdkYmlFile),
	)
	if err != nil {
		fmt.Printf("NewChainClient failed, err:%v\n", err)
		return
	}

	fmt.Println("====================== 发送交易，调用GASM合约 ======================")
	resp, err := cc.InvokeContract("BalanceStable", "Plus", "", map[string]string{"number": "1"}, -1, false)
	if err != nil {
		fmt.Printf("InvokeContract failed, err:%v\n", err)
		return
	}
	respStr, err := json.Marshal(resp)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v\n", err)
		return
	}
	fmt.Printf("%s", respStr)
}

func createEVMContract() {
	// create chainmaker sdk client
	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(sdkYmlFile),
	)
	if err != nil {
		fmt.Printf("new ChainClient failed, err:%v\n", err)
		return
	}

	// create node
	node := createNode(nodeAddress, 5)

	// create admin client
	adminClient, err := sdk.NewChainClient(
		sdk.WithChainClientOrgId(orgId),
		sdk.WithChainClientChainId(chainId),
		sdk.WithUserKeyFilePath(fmt.Sprintf(adminKeyPath, orgId)),
		sdk.WithUserCrtFilePath(fmt.Sprintf(adminCrtPath, orgId)),
		sdk.AddChainClientNodeConfig(node),
	)
	if err != nil {
		fmt.Printf("new adminClient failed, err:%v\n", err)
		return
	}

	fmt.Println("====================== 创建EVM合约 ======================")
	contractName := "balance007"
	version := "1.0.0"
	byteCodePath, _ := filepath.Abs("../contract/ledger_balance.bin")
	byteCode, err := ioutil.ReadFile(byteCodePath)
	if err != nil {
		fmt.Printf("ReadContractFile failed, err:%v\n", err)
		return
	}

	payloadBytes, err := cc.CreateContractCreatePayload(contractName, version, string(byteCode),
		cmCommonPb.RuntimeType_EVM, []*cmCommonPb.KeyValuePair{})
	if err != nil {
		fmt.Printf("CreateContractCreatePayload failed, err:%v\n", err)
		return
	}

	// 各组织Admin权限用户签名
	signedPayloadBytes1, err := adminClient.SignContractManagePayload(payloadBytes)
	if err != nil {
		fmt.Printf("SignContractManagePayload failed, err:%v\n", err)
		return
	}

	// 收集并合并签名
	mergeSignedPayloadBytes, err := cc.MergeContractManageSignedPayload([][]byte{signedPayloadBytes1})
	if err != nil {
		fmt.Printf("MergeContractManageSignedPayload failed, err:%v\n", err)
		return
	}

	// 发送创建合约请求
	resp, err := cc.SendContractManageRequest(mergeSignedPayloadBytes, createContractTimeout, true)
	if err != nil {
		fmt.Printf("SendContractManageRequest failed, err:%v\n", err)
		return
	}
	respStr, err := json.Marshal(resp)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v\n", err)
		return
	}
	fmt.Printf("%s", respStr)
}

func sendEVMTx() {
	// 1.构造客户端
	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(sdkYmlFile),
	)
	if err != nil {
		fmt.Printf("NewChainClient failed, err:%v\n", err)
		return
	}

	// 2.构造方法名和参数
	abiPath, _ = filepath.Abs("../contract/ledger_balance.abi")
	abiJson, err := ioutil.ReadFile(abiPath)
	if err != nil {
		fmt.Printf("ReadABIPathFile failed, err:%v\n", err)
		return
	}

	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
	if err != nil {
		fmt.Printf("abiJSON failed, err:%v\n", err)
		return
	}

	address := evmutils.BigToAddress(evmutils.FromDecimalString(address))

	dataByte, err := myAbi.Pack("updateBalance", big.NewInt(int64(balance)), address)
	if err != nil {
		fmt.Printf("abiJPack failed, err:%v\n", err)
		return
	}

	dataString := hex.EncodeToString(dataByte)
	method := dataString[0:8]

	pairs := map[string]string{
		"data": dataString,
	}

	fmt.Println("====================== 发送交易，调用EVM合约 ======================")
	resp, err := cc.InvokeContract("balance007", method, "", pairs, -1, false)
	if err != nil {
		fmt.Printf("InvokeContract failed, err:%v\n", err)
		return
	}
	respStr, err := json.Marshal(resp)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v\n", err)
		return
	}
	fmt.Printf("%s", respStr)
}

func getTxInfo() {

	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(sdkYmlFile),
	)
	if err != nil {
		fmt.Printf("NewChainClient failed, err:%v\n", err)
		return
	}

	block, err := cc.GetBlockByHeight(int64(height), false)
	if err != nil {
		fmt.Printf("GetBlockByHeight failed, err:%v\n", err)
		return
	}
	contractInfo, err := utils.GetContractInfoByTxPayload(block.Block.Txs[0])
	if err != nil {
		fmt.Printf("GetContractInfoByTxPayload failed, err:%v\n", err)
		return
	}
	fmt.Printf("=======================查询交易信息=======================\n")
	fmt.Printf("tx code: %d\n", block.Block.Txs[0].Result.Code)
	fmt.Printf("tx key: %s\n", block.Block.Txs[0].Header.TxId)
	fmt.Printf("contract info: %v\n", contractInfo)
}

func verifyTransaction() {
	// 1.构造Client
	conn, err := grpc.Dial(grpcAP, grpc.WithInsecure())
	if err != nil {
		fmt.Printf("grpcDial failed, err:%v\n", err)
		return
	}
	defer conn.Close()
	client := api.NewRpcProverClient(conn)

	// 2.读取配置文件中的交易信息
	cmViper := viper.New()
	cmViper.SetConfigFile(txInfoPath)
	if err = cmViper.ReadInConfig(); err != nil {
		fmt.Printf("read txInfoPath error: %v\n", err)
		return
	}
	txInfo := &TxInfo{}
	if err = cmViper.Unmarshal(txInfo); err != nil {
		fmt.Printf("unmarshal txInfo error: %v\n", err)
		return
	}

	// 3.构造ContractValidData
	contractData := &api.ContractData{
		ContractName: txInfo.ContractData.ContractName,
		Method:       txInfo.ContractData.Method,
		Extra:        nil,
	}

	for _, param := range txInfo.ContractData.Params {
		pair := &api.KVPair{
			Key:   param.Key,
			Value: param.Value,
		}
		contractData.Params = append(contractData.Params, pair)
	}

	// 如果是调用EVM合约的交易,对Method和Params进行编码
	if runTime == int(cmCommonPb.RuntimeType_EVM) {
		if abiPath == "" {
			fmt.Printf("abiPath should not be \"\"")
			return
		}
		var abs string
		abs, err = filepath.Abs(abiPath)
		if err != nil {
			abiPath = abs
		}

		codeMethodAndParams(contractData, abiPath)
	}

	// 4.构造TxValidationInfo
	txValidationInfo := &api.TxValidationInfo{
		ChainId:      txInfo.ChainId,
		BlockHeight:  int64(txInfo.BlockHeight),
		Index:        -1,
		TxKey:        txInfo.TxKey,
		ContractData: contractData,
		Extra:        nil,
	}

	// 5.交易有效性证明
	fmt.Println("====================== 交易有效性验证 ======================")
	response, _ := client.ValidTransaction(context.Background(), txValidationInfo)
	responseStr, err := json.Marshal(response)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v", err)
	}
	fmt.Printf("%s", responseStr)
}

func blockUpdate() {
	// create chainmaker sdk client
	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(sdkYmlFile),
	)
	if err != nil {
		fmt.Printf("new ChainClient failed, err:%v\n", err)
		return
	}

	// create node
	node := createNode(nodeAddress, 5)

	// create admin client
	adminClient, err := sdk.NewChainClient(
		sdk.WithChainClientOrgId(orgId),
		sdk.WithChainClientChainId(chainId),
		sdk.WithUserKeyFilePath(fmt.Sprintf(adminKeyPath, orgId)),
		sdk.WithUserCrtFilePath(fmt.Sprintf(adminCrtPath, orgId)),
		sdk.AddChainClientNodeConfig(node),
	)
	if err != nil {
		fmt.Printf("new adminClient failed, err:%v\n", err)
		return
	}

	fmt.Println("====================== 更新区块配置的交易 ======================")
	isVerify := rand.Intn(2) == 0
	txTimeout := rand.Intn(1000) + 600
	capacity := rand.Intn(1000) + 1
	size := rand.Intn(10) + 1
	interval := rand.Intn(10000) + 10

	payloadBytes, err := cc.CreateChainConfigBlockUpdatePayload(isVerify, txTimeout, capacity, size, interval)
	if err != nil {
		fmt.Printf("CreateChainConfigBlockUpdatePayload failed, err:%v\n", err)
		return
	}

	// 各组织Admin权限用户签名
	signedPayloadBytes1, err := adminClient.SignChainConfigPayload(payloadBytes)
	if err != nil {
		fmt.Printf("SignChainConfigPayload failed, err:%v\n", err)
		return
	}

	// 收集并合并签名
	mergeSignedPayloadBytes, err := cc.MergeChainConfigSignedPayload([][]byte{signedPayloadBytes1})
	if err != nil {
		fmt.Printf("MergeChainConfigSignedPayload failed, err:%v\n", err)
		return
	}

	// 发送请求
	resp, err := cc.SendChainConfigUpdateRequest(mergeSignedPayloadBytes)
	if err != nil {
		fmt.Printf("SendChainConfigUpdateRequest failed, err:%v\n", err)
		return
	}
	respStr, err := json.Marshal(resp)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v\n", err)
		return
	}
	fmt.Printf("%s", respStr)
}

func codeMethodAndParams(contractData *api.ContractData, abiPath string) {
	if contractData == nil || contractData.Method == "" || contractData.Params == nil {
		fmt.Printf("ContractValidData should not be nill\n")
		return
	}

	// 编码method和params
	abiJson, err := ioutil.ReadFile(abiPath)
	if err != nil {
		fmt.Printf("Read abiPath failed, err:%s\n", err.Error())
		return
	}

	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
	if err != nil {
		fmt.Printf("abiJSON failed, err:%s\n", err.Error())
		return
	}

	var (
		method  string
		address string
		balance string
	)
	for _, pair := range contractData.Params {
		if pair.Key == "address" {
			address = pair.Value
		}
		if pair.Key == "balance" {
			balance = pair.Value
		}
	}

	addr := evmutils.BigToAddress(evmutils.FromDecimalString(address))

	balanceInt64, _ := strconv.ParseInt(balance, 10, 64)
	dataByte, err := myAbi.Pack(contractData.Method, big.NewInt(balanceInt64), addr)
	if err != nil {
		fmt.Printf("Params coding failed, err:%s\n", err.Error())
		return
	}

	dataString := hex.EncodeToString(dataByte)
	method = dataString[0:8]

	pair := &api.KVPair{
		Key:   "data",
		Value: dataString,
	}
	parameters := make([]*api.KVPair, 0)
	parameters = append(parameters, pair)

	// 更新contractData
	contractData.Method = method
	contractData.Params = parameters
}
