/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package main

type TxInfo struct {
	ChainId      string        `mapstructure:"chain_id"`
	BlockHeight  int           `mapstructure:"block_height"`
	TxKey        string        `mapstructure:"tx_key"`
	ContractData *ContractData `mapstructure:"contract_valid_data"`
}

type ContractData struct {
	ContractName string    `mapstructure:"contract_name"`
	Method       string    `mapstructure:"method"`
	Params       []*Params `mapstructure:"parameters"`
}

type Params struct {
	Key   string `mapstructure:"key"`
	Value string `mapstructure:"value"`
}
