## SPV tools使用说明

### 前置需求：

- 部署业务链
- 部署SPV

上述需求请参考SPV部署文档 - [快速部署文档](../../docs/deploy_manual.md)


### SPV中tools使用方式：
- 将build/release/config中的crypto-config复制到tools/config目录下
- 修改chainmaker_sdk_config_chain1.yml中节点ip/port 
- 修改main.go中sdk需要配置的内容，如chainId、orgId、nodeAddress、sdkYmlFile
- 编译tools中的main.go, 生成main可执行文件

```shell script
## 部署GASM合约
./main -step 0


## 调用GASM合约，发送交易   
./main -step 1


## 部署EVM合约
./main -step 2


## 调用EVM合约，发送交易   
./main -step 3 -addr="" -balance=x           // addr需要传入数字类型的的字符串


## 获取某一高度区块中第一笔交易的状态码和TxId    
./main -step 4 -height=h                     // h是需要输入的区块高度


## GASM合约交易验证  需要在tools/config/params.yml中填写验证的交易信息
./main -step 5 -addr_port="127.0.0.1:12308" -tx_info_path="../config/params.yml" -run_time=4


## EVM合约交易验证  需要在tools/config/params.yml中填写验证的交易信息
./main -step 5 -addr_port="127.0.0.1:12308" -tx_info_path="../config/params.yml" -run_time=5 -abi_path="../contract/ledger_balance.abi"


# 交易有效性验证返回值
{"ChainId":"chain1","TxKey":"3dc6fc2c91564103835800e13f8a3d761b23dd7008c64cb7a4f530082b4e7623","Code":0,"Message":"valid transaction"}%

其中：
ChainId：链Id
TxKey: 交易Id
Code: 状态码,交易有效为 0, 交易无效为 1
Message: 提示信息
```