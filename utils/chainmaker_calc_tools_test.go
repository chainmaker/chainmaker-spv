/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package utils

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/conf"
	"chainmaker.org/chainmaker-spv/pb/protogo"
	"github.com/gogo/protobuf/proto"
	"github.com/stretchr/testify/require"
)

func Test_CalcDagHash(t *testing.T) {
	hb, err := CalcDagHash("SHA256", &cmCommonPb.DAG{})
	require.NotNil(t, hb)
	require.Nil(t, err)
}

func Test_CalcRWSetRoot(t *testing.T) {
	ht := "SHA256"
	tx1 := &cmCommonPb.Transaction{
		Result: &cmCommonPb.Result{
			RwSetHash: []byte("dwsdefdwdfdwq3ddedsqw"),
		},
	}
	tx2 := &cmCommonPb.Transaction{
		Result: &cmCommonPb.Result{
			RwSetHash: []byte("dwsdefdwdfdwq3ddedsqw"),
		},
	}
	tx3 := &cmCommonPb.Transaction{
		Result: &cmCommonPb.Result{
			RwSetHash: []byte("dwsdefdwdfdwq3ddedsqw"),
		},
	}

	root, err := CalcRWSetRoot(ht, []*cmCommonPb.Transaction{tx1, tx2, tx3})
	require.NotNil(t, root)
	require.Nil(t, err)
}

func Test_CalcTxHash(t *testing.T) {
	conf.BlockChainConfigs["chain1"] = &protogo.ChainConfig{
		HashType: "SHA256",
	}
	th, err := CalcTxHash(&cmCommonPb.Transaction{
		Header: &cmCommonPb.TxHeader{
			ChainId: "chain1",
		},
	})
	require.NotNil(t, th)
	require.Nil(t, err)
}

func Test_NewContractInfo(t *testing.T) {
	ci := NewContractInfo("contractname", "method", make([]*cmCommonPb.KeyValuePair, 0))
	require.NotNil(t, ci)
}

func Test_GetContractInfoByTxPayload(t *testing.T) {
	tran := &cmCommonPb.Transaction{
		Header: &cmCommonPb.TxHeader{
			ChainId: "chain1",
		},
		Result: &cmCommonPb.Result{
			Code:           cmCommonPb.TxStatusCode_SUCCESS,
			ContractResult: &cmCommonPb.ContractResult{},
		},
	}
	payload := newRequestPayload(cmCommonPb.TxType_INVOKE_USER_CONTRACT)
	tran.RequestPayload = payload
	tran.Header.TxType = cmCommonPb.TxType_INVOKE_USER_CONTRACT
	ci, err := GetContractInfoByTxPayload(tran)
	require.NotNil(t, ci)
	require.Nil(t, err)

	payload = newRequestPayload(cmCommonPb.TxType_UPDATE_CHAIN_CONFIG)
	tran.RequestPayload = payload
	tran.Header.TxType = cmCommonPb.TxType_UPDATE_CHAIN_CONFIG
	ci, err = GetContractInfoByTxPayload(tran)
	require.NotNil(t, ci)
	require.Nil(t, err)

	payload = newRequestPayload(cmCommonPb.TxType_MANAGE_USER_CONTRACT)
	tran.RequestPayload = payload
	tran.Header.TxType = cmCommonPb.TxType_MANAGE_USER_CONTRACT
	ci, err = GetContractInfoByTxPayload(tran)
	require.NotNil(t, ci)
	require.Nil(t, err)
}

func newRequestPayload(txType cmCommonPb.TxType) []byte {
	name := "contractName"
	method := "method"
	param := &cmCommonPb.KeyValuePair{
		Key:   "key",
		Value: "value",
	}

	switch txType {
	case cmCommonPb.TxType_INVOKE_USER_CONTRACT:
		rp := &cmCommonPb.TransactPayload{
			ContractName: name,
			Method:       method,
			Parameters: []*cmCommonPb.KeyValuePair{
				param,
			},
		}
		rpBytes, err := proto.Marshal(rp)
		if err != nil {
			return nil
		}
		return rpBytes

	case cmCommonPb.TxType_UPDATE_CHAIN_CONFIG, cmCommonPb.TxType_INVOKE_SYSTEM_CONTRACT:
		rp := &cmCommonPb.SystemContractPayload{
			ContractName: name,
			Method:       method,
			Parameters: []*cmCommonPb.KeyValuePair{
				param,
			},
		}
		rpBytes, err := proto.Marshal(rp)
		if err != nil {
			return nil
		}
		return rpBytes

	case cmCommonPb.TxType_MANAGE_USER_CONTRACT:
		rp := &cmCommonPb.ContractMgmtPayload{
			ContractId: &cmCommonPb.ContractId{
				ContractName: name,
			},
			Method: method,
			Parameters: []*cmCommonPb.KeyValuePair{
				param,
			},
		}
		rpBytes, err := proto.Marshal(rp)
		if err != nil {
			return nil
		}
		return rpBytes

	default:
		return nil
	}
}
