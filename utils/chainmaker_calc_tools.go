/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package utils defines tools for ChainMaker spv
package utils

import (
	"errors"
	"fmt"

	"chainmaker.org/chainmaker-go/common/crypto/hash"
	cmCommonPb "chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"chainmaker.org/chainmaker-spv/conf"
	"github.com/gogo/protobuf/proto"
)

// CalcDagHash calculate DAG hash
func CalcDagHash(hashType string, dag *cmCommonPb.DAG) ([]byte, error) {
	if dag == nil {
		return nil, errors.New("calc hash block == nil")
	}

	dagBytes, err := proto.Marshal(dag)
	if err != nil {
		return nil, fmt.Errorf("marshal DAG error, %s", err)
	}

	hashByte, err := hash.GetByStrType(hashType, dagBytes)
	if err != nil {
		return nil, err
	}
	return hashByte, nil
}

// CalcRWSetRoot calculate txs' read-write set root hash, following the tx order in txs
func CalcRWSetRoot(hashType string, txs []*cmCommonPb.Transaction) ([]byte, error) {
	// calculate read-write set hash following the order in txs
	// if txId does not exist in txRWSetMap, fill in a default one
	if len(txs) == 0 {
		return nil, errors.New("calc rwset root set == nil")
	}
	rwSetHashes := make([][]byte, len(txs))
	for i, tx := range txs {
		rwSetHashes[i] = tx.Result.RwSetHash
	}

	// calculate the merkle root
	root, err := hash.GetMerkleRoot(hashType, rwSetHashes)
	return root, err
}

// CalcTxHash calculate transaction hash
func CalcTxHash(transaction *cmCommonPb.Transaction) ([]byte, error) {
	if transaction == nil {
		return nil, errors.New("transaction is nil")
	}
	txBytes, err := transaction.Marshal()
	if err != nil {
		return nil, err
	}

	return hash.GetByStrType(conf.BlockChainConfigs[transaction.Header.ChainId].HashType, txBytes)
}

// ContractInfo contains contract data in the block
type ContractInfo struct {
	ContractName string
	Method       string
	Parameters   []interface{}
}

// NewContractInfo creates ContractInfo by contract name、contract method and contract params
func NewContractInfo(name, method string, params []*cmCommonPb.KeyValuePair) *ContractInfo {
	ci := &ContractInfo{
		ContractName: name,
		Method:       method,
	}

	l := len(params)
	parameters := make([]interface{}, l)
	for i := 0; i < l; i++ {
		parameters[i] = params[i]
	}
	ci.Parameters = parameters

	return ci
}

// GetContractInfoByTxPayload parses RequestPayload in the Transaction to generate ContractInfo
// which contains contract name、method and params
func GetContractInfoByTxPayload(transaction *cmCommonPb.Transaction) (*ContractInfo, error) {
	txType := transaction.Header.TxType
	switch txType {
	case cmCommonPb.TxType_INVOKE_USER_CONTRACT:
		payload := &cmCommonPb.TransactPayload{}
		err := proto.Unmarshal(transaction.RequestPayload, payload)
		if err != nil {
			return nil, err
		}
		return NewContractInfo(payload.ContractName, payload.Method, payload.Parameters), nil

	case cmCommonPb.TxType_UPDATE_CHAIN_CONFIG, cmCommonPb.TxType_INVOKE_SYSTEM_CONTRACT:
		payload := &cmCommonPb.SystemContractPayload{}
		err := proto.Unmarshal(transaction.RequestPayload, payload)
		if err != nil {
			return nil, err
		}
		return NewContractInfo(payload.ContractName, payload.Method, payload.Parameters), nil

	case cmCommonPb.TxType_MANAGE_USER_CONTRACT:
		payload := &cmCommonPb.ContractMgmtPayload{}
		err := proto.Unmarshal(transaction.RequestPayload, payload)
		if err != nil {
			return nil, err
		}
		return NewContractInfo(payload.ContractId.ContractName, payload.Method, payload.Parameters), nil

	default:
		return nil, errors.New("invalid transaction type")
	}
}
