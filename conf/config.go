/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package conf contains the logic to read and initialize system configuration
package conf

import (
	"path/filepath"

	"chainmaker.org/chainmaker-spv/logger"
	"chainmaker.org/chainmaker-spv/pb/protogo"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	flagSets          = make([]*pflag.FlagSet, 0)             // flag set
	Filepath          = "../config/spv_config.yml"            // default config file path
	SPVConfig         = &Config{}                             // global config containing all chains' spv config
	BlockChainConfigs = make(map[string]*protogo.ChainConfig) // all remote chains' config got by sdk
)

// InitSPVConfig inits SPVConfig when deployed independently
func InitSPVConfig(cmd *cobra.Command) error {
	// 1. init config
	config, err := initSPVConfig(cmd)
	if err != nil {
		return err
	}

	// 2. set sdk config path to abs path
	for i := 0; i < len(config.Chains); i++ {
		if !filepath.IsAbs(config.Chains[i].SDKConfigPath) {
			config.Chains[i].SDKConfigPath, _ = filepath.Abs(config.Chains[i].SDKConfigPath)
		}
	}

	// 3. set log config
	logger.SetLogConfig(config.LogConfig)

	// 4. set global config
	SPVConfig = config

	return nil
}

// InitSPVConfigWithYmlFile inits SPVConfig when integrated as a component
func InitSPVConfigWithYmlFile(ymlFile string) error {
	// 1. init spv config
	config, err := initSPVConfigWithYmlFile(ymlFile)
	if err != nil {
		return err
	}

	// 2. set sdk config path to abs path
	for i := 0; i < len(config.Chains); i++ {
		if !filepath.IsAbs(config.Chains[i].SDKConfigPath) {
			config.Chains[i].SDKConfigPath, _ = filepath.Abs(config.Chains[i].SDKConfigPath)
		}
	}

	// 3. set global config
	SPVConfig = config

	return nil
}

func initSPVConfig(cmd *cobra.Command) (*Config, error) {
	// 1. load env
	cmViper := viper.New()
	err := cmViper.BindPFlags(cmd.PersistentFlags())
	if err != nil {
		return nil, err
	}

	// 2. load the path of the config files
	ymlFile := Filepath
	if !filepath.IsAbs(ymlFile) {
		ymlFile, _ = filepath.Abs(ymlFile)
		Filepath = ymlFile
	}

	// 3. load the config file
	cmViper.SetConfigFile(ymlFile)
	if err := cmViper.ReadInConfig(); err != nil {
		return nil, err
	}

	for _, command := range cmd.Commands() {
		flagSets = append(flagSets, command.PersistentFlags())
		err := cmViper.BindPFlags(command.PersistentFlags())
		if err != nil {
			return nil, err
		}
	}

	// 4. new spv config instance
	config := &Config{}
	if err := cmViper.Unmarshal(config); err != nil {
		return nil, err
	}

	return config, nil
}

func initSPVConfigWithYmlFile(ymlFile string) (*Config, error) {
	cmViper := viper.New()

	// 1. load the path of the config files
	if ymlFile == "" {
		ymlFile = Filepath
	}

	if !filepath.IsAbs(ymlFile) {
		ymlFile, _ = filepath.Abs(ymlFile)
		Filepath = ymlFile
	}

	// 2. load the config file
	cmViper.SetConfigFile(ymlFile)
	if err := cmViper.ReadInConfig(); err != nil {
		return nil, err
	}

	// 3. new spv config instance
	config := &Config{}
	if err := cmViper.Unmarshal(config); err != nil {
		return nil, err
	}

	return config, nil
}
