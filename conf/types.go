/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package conf

import (
	"chainmaker.org/chainmaker-spv/logger"
)

// Config is the config containing all chains' spv config
type Config struct {
	Chains        []*ChainConfig    `mapstructure:"chain"`
	GRPCConfig    *GRPCConfig       `mapstructure:"grpc"`
	StorageConfig *StoreConfig      `mapstructure:"storage"`
	LogConfig     *logger.LogConfig `mapstructure:"log"`
}

// ChainConfig is the config of one chain's spv config
type ChainConfig struct {
	ChainId                    string `mapstructure:"chain_id"`
	SyncChainInfoIntervalMills int32  `mapstructure:"sync_chainInfo_interval"`
	SDKConfigPath              string `mapstructure:"sdk_config_path"`
}

// GRPCConfig is the config of gRPC module
type GRPCConfig struct {
	Address string `mapstructure:"address"`
	Port    int    `mapstructure:"port"`
}

// StoreConfig is the config of storage module
type StoreConfig struct {
	Provider string         `mapstructure:"provider"`
	LevelDB  *LevelDBConfig `mapstructure:"leveldb"`
}

// LevelDBConfig is the config of leveldb module
type LevelDBConfig struct {
	StorePath       string `mapstructure:"store_path"`
	WriteBufferSize int    `mapstructure:"write_buffer_size"`
	BloomFilterBits int    `mapstructure:"bloom_filter_bits"`
}
